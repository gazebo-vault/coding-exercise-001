# Statistics-API

Prerequisites:
--------------
1. JDK 1.8 (higher versions may have issues with the implemented version of Jacoco)
2. Intellij Idea/ Eclipse IDE

Setting up:
-------------
1. Configure JAVA_HOME environment variables.
2. Clone the repository, `cd` into the directory and run `./gradlew`. This will fetch all the dependencies.
3. './gradlew clean build` for project build
4. './gradlew bootrun' to run 

Environment Variable:
---------------------
 Environment Selection for Resources
 `e.g. 'environment' : 'dev'`
 
 Support For External Resources
 `e.g. 'data.dir' : '/opt/tools/app/stats/config'`
 
 Logs Path
 `e.g. 'api.log.path': '/opt/tools/app/stats/logs'`

Run Task
-------------------------------
* bootRun - used for running on the developer machine
* test - used in unit/integration tests

Running:
-------------
1. Use the command './gradlew clean build' for build&nbsp;
2. Use the command ./gradlew bootRun' to run application. Change systemProp.bootjar=false&nbsp;
3. Use the command './gradlew bootRun --debug-jvm' for debug&nbsp;
4. Use the following command to run the app as standalone bootjar. Change systemProp.bootjar=true and clean build before running:&nbsp;
java -Denvironment=dev -jar build/libs/statistics-api*.jar

Swagger UI:
-------------
The URL for Swagger on the local machine is `http://localhost:8080/api/v1/swagger-ui.html`.

API Contract:
-------------
The API contract can be generated using the following link:
`http://localhost:8080/api/v1/v2/api-docs`

Docker:
-------------
1. Start docker engine.
2. Open terminal and go to the project folder.
3. Run the following command:
docker build -t prbsmart/statistics-api . 

Image is already uploaded to dockerhub. Only running the below command will bring the app up and running:

4. docker run -d -p 8080:8080 -e env=dev prbsmart/statistics-api

Notes:
-------------
1. The api resources are secured. Trying them directly from swagger will not work. Please try them from browser instead,
to open the login screen. Each resource can be accessed by a user belonging to a specific group.
2. Properties file has few dummy users and admins belonging to certain groups. In real life scenario this can be
implemented using a database or active directory. Oauth2.0 providers like Okta can also be used to generate security
tokens configurable in application.yml
3. The cvs file with the user details is placed in resources. The file is accessed via dao. If the data goes to database,
the csv file will be replaced with user tables and only the dao layer will be modified. Right now the data is cached 
since its not changing. In future, the dao layer can be taken out to create a DAAS (Data As A Service) for greater 
reasons.
4. Apache Spark has been used for Bucketization
5. For api development, bottom up approach has been taken i.e. models are created and the api contract can ge generated
from the api. Instead, top down approach can also be taken i.e. contract can be created in the swagger editor/swaggerhub
and models can be generated one time or during each build.
6. For the sake of simplicity, console logging is used and resources are kept inside the app. Instead, config server
can be leveraged for externalizing props and spring cloud bus can be used fot real time refresh.

Api resources:
-------------
1. http://localhost:8080/api/v1/statistics (username=admin1, password=password2, role=StatsAdmin)
2. http://localhost:8080/api/v1/bucketdata?columnName=age&bucket=5 (username=user1, password=password1, role=StatsUser)
3. http://localhost:8080/api/v1/bucketdata?columnName=firstName&bucket=5 (username=user1, password=password1, role=StatsUser)
4. http://localhost:8080/api/v1/match?firstName=darwin (username=user1, password=password1, role=StatsUser)

