package com.company.stats;

import com.company.stats.commons.Constants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Configuration
public class SwaggerDocumentationConfig {

    /**
     * @return
     */
    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(Constants.API_TITLE)
                .description(Constants.API_DESCRIPTION)
                .version(Constants.VERSION)
                .contact(new Contact("", "", Constants.CONTACT_EMAIL))
                .build();
    }

    /**
     * @return
     */
    @Bean
    public Docket apiDocket() {
        ParameterBuilder parameterBuilder = new ParameterBuilder();
        List<Parameter> parameters = new ArrayList<>();
        Parameter transactionId = parameterBuilder.name(Constants.TRANSACTION_ID)
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .defaultValue("")
                .required(false)
                .build();
        parameters.add(transactionId);
        Parameter deviceId = parameterBuilder.name(Constants.DEVICE_ID)
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .defaultValue("")
                .required(false)
                .build();
        parameters.add(deviceId);
        Parameter application = parameterBuilder.name(Constants.APPLICATION)
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .defaultValue("")
                .required(false)
                .build();
        parameters.add(application);
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .select()
                .apis(RequestHandlerSelectors.basePackage(Constants.BASE_CONTROLLER_PACKAGE))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(parameters);
    }

}
