package com.company.stats;

import com.company.stats.commons.CommonUtils;
import com.company.stats.commons.Constants;
import com.company.stats.core.Context;
import com.company.stats.core.LoggerContext;
import org.apache.logging.log4j.ThreadContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 *
 */
@Component
public class ApplicationFilter implements Filter {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationFilter.class);

    public static final String TRANSACTION_ID = "TRANSACTION_ID";
    public static final String APP_TRANSACTION_ID = "APP_TRANSACTION_ID";

    /**
     * @param fConfig
     */
    public void init(FilterConfig fConfig) {

    }

    /**
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        Context context = CommonUtils.getContext(httpServletRequest);
        httpServletRequest.setAttribute(Constants.REQUEST_LOG_CONTEXT, context);
        //Set Transaction Id
        setTransactionId(context);
        chain.doFilter(request, response);
        stopWatch.stop();
        analyticsLogging(httpServletRequest, stopWatch);
    }

    /**
     * @param context
     */
    public void setTransactionId(Context context) {
        ThreadContext.put(TRANSACTION_ID, context.getTransactionId());
        setAppTransactionId(context);
    }

    /**
     * @param context
     */
    public void setAppTransactionId(Context context) {
        ThreadContext.put(APP_TRANSACTION_ID, context.getTransactionId());
    }

    /**
     *
     */
    public void destroy() {
    }

    /**
     * @param request
     * @param stopWatch
     */
    private void analyticsLogging(HttpServletRequest request, StopWatch stopWatch) {
        if (request != null) {
            Object object = request.getAttribute(Constants.LOG_TYPE_ANALYTICS);
            if (object instanceof LoggerContext) {
                LoggerContext loggerContext = (LoggerContext) object;
                loggerContext.setType(Constants.LOG_TYPE_ANALYTICS);
                loggerContext.setElapsedTime(stopWatch != null ? String.valueOf(stopWatch.getTotalTimeMillis()) : "0");
                loggerContext.setMethodName(null);
                logger.info(loggerContext.toString());
            }

        }
    }

}
