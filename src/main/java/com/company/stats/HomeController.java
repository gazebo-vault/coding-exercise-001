package com.company.stats;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 */
@Controller
public class HomeController {

    public static final String REDIRECT_SWAGGER_UI_HTML = "redirect:swagger-ui.html";

    /**
     * @return
     */
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String index() {
        return REDIRECT_SWAGGER_UI_HTML;
    }
}
