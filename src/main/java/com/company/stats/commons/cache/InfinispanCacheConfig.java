package com.company.stats.commons.cache;

import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.cache.ExpirationConfigurationBuilder;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.spring.embedded.provider.SpringEmbeddedCacheManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.cache.interceptor.CacheResolver;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 *
 */
@Configuration
@EnableCaching
public class InfinispanCacheConfig implements CachingConfigurer {

    @Value("${cache.expirytime}")
    private int expiryTimeInMinute;

    /**
     * @return
     */
    @Override
    public CacheManager cacheManager() {
        EmbeddedCacheManager cacheManager = new DefaultCacheManager();
        ExpirationConfigurationBuilder builder = new ConfigurationBuilder().simpleCache(true)
                .expiration().lifespan(expiryTimeInMinute, TimeUnit.MINUTES);
        cacheManager.defineConfiguration(CacheConstants.CACHE_NAME, builder.build());
        return new SpringEmbeddedCacheManager(cacheManager);
    }

    /**
     * @return
     */
    @Override
    public CacheResolver cacheResolver() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @return
     */
    @Override
    public KeyGenerator keyGenerator() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @return
     */
    @Override
    public CacheErrorHandler errorHandler() {
        // TODO Auto-generated method stub
        return null;
    }

}
