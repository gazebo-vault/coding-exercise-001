package com.company.stats.commons.cache;

import javax.cache.annotation.GeneratedCacheKey;

/**
 *
 */
public class StringCacheKey implements GeneratedCacheKey {

    private String value;

    /**
     * @param param
     */
    public StringCacheKey(String param) {
        this.value = param;
    }

    /**
     * @return
     */
    @Override
    public int hashCode() {
        return this.value.hashCode();
    }

    /**
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        return obj != null && obj.getClass() == this.getClass() && this.value.equals(((StringCacheKey) obj).getValue());
    }

    /**
     * @return
     */
    public String getValue() {
        return value;
    }
}
