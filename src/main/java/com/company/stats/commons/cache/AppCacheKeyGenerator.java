package com.company.stats.commons.cache;

import javax.cache.annotation.CacheInvocationParameter;
import javax.cache.annotation.CacheKeyInvocationContext;
import javax.cache.annotation.GeneratedCacheKey;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 *
 */
public class AppCacheKeyGenerator implements javax.cache.annotation.CacheKeyGenerator {

    /**
     * @param cacheKeyInvocationContext
     * @return
     */
    @Override
    public GeneratedCacheKey generateCacheKey(final CacheKeyInvocationContext<?
            extends Annotation> cacheKeyInvocationContext) {

        final Method method = cacheKeyInvocationContext.getMethod();
        final CacheInvocationParameter[] allParameters = cacheKeyInvocationContext.getAllParameters();
        String paramStr = "";
        for (CacheInvocationParameter obj : allParameters) {
            if (obj.getValue() instanceof String) {
                paramStr += (String) obj.getValue();
            }
        }
        String methodName = method.getName();
        String key;
        if (paramStr == null) {
            key = methodName.trim();
        } else {
            key = new StringBuilder().append(methodName.trim())
                    .append(CacheConstants.SEPARATOR).append(paramStr.trim()).toString();
        }
        return new StringCacheKey(key);

    }
}
