package com.company.stats.commons;

import org.apache.spark.ml.feature.Bucketizer;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class BucketizerUtils {

    /**
     *
     * @param values
     * @param bucket
     * @return
     */
    public static List<Row> bucketize(List<Double> values, Integer bucket) {
        List<Row> data = new ArrayList<>();
        for (Double value : values) {
            data.add(RowFactory.create(value));
        }
        SparkSession spark = SparkSession
                .builder()
                .appName(Constants.BUCKETIZER_APP)
                .config(Constants.SPARK_MASTER, Constants.LOCAL)
                .getOrCreate();
        //double[] splits = {1, 20, 40, 60, 80, Double.POSITIVE_INFINITY};
        int increment = (int) (values.get(values.size() - 1) / bucket);
        double[] splits = new double[bucket + 1];
        for (int i = 1; i < bucket; i++) {
            splits[i] = (double)increment * i;
        }
        splits[0] = Double.NEGATIVE_INFINITY;
        splits[bucket] = Double.POSITIVE_INFINITY;
        StructType schema = new StructType(new StructField[]{
                new StructField(Constants.BUCKET_INPUT_FIELD, DataTypes.DoubleType, false, Metadata.empty())
        });
        Dataset<Row> dataFrame = spark.createDataFrame(data, schema);
        Bucketizer bucketizer = new Bucketizer()
                .setInputCol(Constants.BUCKET_INPUT_FIELD)
                .setOutputCol(Constants.BUCKET_OUTPUT_FIELD)
                .setSplits(splits);
        // Transform original data into its bucket index.
        Dataset<Row> bucketedData = bucketizer.transform(dataFrame);
        //bucketedData.show();
        spark.stop();
        return bucketedData.collectAsList();
    }
}


