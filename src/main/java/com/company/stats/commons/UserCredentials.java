package com.company.stats.commons;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.util.Map;

/**
 *
 */
@EnableConfigurationProperties
@ConfigurationProperties(prefix = Constants.AUTHENTICATION_PREFIX)
public class UserCredentials {

    @Value("${authority.group.user}")
    private String userGroup;

    @Value("${authority.group.admin}")
    private String adminGroup;

    private Map<String, String> users;
    private Map<String, String> admin;

    /**
     * @return
     */
    public String getUserGroup() {
        return userGroup;
    }

    /**
     * @param userGroup
     */
    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }

    /**
     * @return
     */
    public String getAdminGroup() {
        return adminGroup;
    }

    /**
     * @param adminGroup
     */
    public void setAdminGroup(String adminGroup) {
        this.adminGroup = adminGroup;
    }

    /**
     * @return
     */
    public Map<String, String> getUsers() {
        return users;
    }

    /**
     * @param users
     */
    public void setUsers(Map<String, String> users) {
        this.users = users;
    }

    /**
     * @return
     */
    public Map<String, String> getAdmin() {
        return admin;
    }

    /**
     * @param admin
     */
    public void setAdmin(Map<String, String> admin) {
        this.admin = admin;
    }
}
