package com.company.stats.commons;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public enum ColumnNameEnum {

    AGE("age"),
    VANTAGE_SORE("vantageScore"),
    FICO_SCORE("ficoScore");

    private final String value;

    /**
     *
     */
    private static final Map<String, ColumnNameEnum> nameIndex =
            new HashMap<>(ColumnNameEnum.values().length);

    /**
     *
     */
    static {
        for (ColumnNameEnum columnName : ColumnNameEnum.values()) {
            nameIndex.put(columnName.getValue(), columnName);
        }
    }

    /**
     *
     * @param value
     */
    ColumnNameEnum(String value) {
        this.value = value;
    }

    /**
     *
     * @return
     */
    public String getValue() {
        return value;
    }

    /**
     *
     * @param name
     * @return
     */
    public static ColumnNameEnum lookupByName(String name) {
        return nameIndex.get(name);
    }

}
