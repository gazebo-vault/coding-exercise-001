package com.company.stats.commons;

/**
 *
 */
public class Constants {

    public static final String APPLICATION_DEFAULT_CLASSPATH_PATH = "classpath:statistics.properties";
    public static final String APPLICATION_DEFAULT_RESOURCE_PATH = "file:${data"
            + ".dir}/properties/statistics/statistics.properties";
    public static final String APPLICATION_ENV_CLASSPATH_PATH = "classpath:statistics-${environment}.properties";
    public static final String APPLICATION_ENV_RESOURCE_PATH = "file:${data"
            + ".dir}/properties/statistics/statistics-${environment}.properties";
    public static final String APPLICATION_LOCAL_CLASSPATH_PATH = "classpath:${local}.properties";
    public static final String APPLICATION_MESSAGE_CLASSPATH_PATH = "classpath:messages.properties";
    public static final String APPLICATION_MESSAGE_RESOURCE_PATH = "file:${data"
            + ".dir}/properties/statistics/messages.properties";
    public static final String APPLICATION_API_CLIENT_CLASSPATH_PATH = "classpath:api-client.properties";
    public static final String APPLICATION_API_CLIENT_ENV_CLASSPATH_PATH = "classpath:api-client-${environment}.properties";
    public static final String APPLICATION_ERROR_MAPPING_CLASSPATH_PATH = "classpath:error-mapping.properties";
    public static final String APPLICATION_ERROR_MAPPING_RESOURCE_PATH = "file:${data" +
            ".dir}/properties/statistics/error-mapping.properties";
    public static final String LOG_TYPE_ANALYTICS = "analytics";

    //Root Package for Component Scan
    public static final String APPLICATION_ROOT_PACKAGE_SCAN = "com.company.stats";

    public static final String API_TITLE = "Statistics API";
    public static final String API_DESCRIPTION = "Statistics API  which returns statistics for all the numeric "
            + "columns, bucketized data for any numeric column and Soundex on the requested data.";
    public static final String VERSION = "1.0.0";
    public static final String BASE_CONTROLLER_PACKAGE = "com.company.stats.api.controller";
    public static final String CONTACT_EMAIL = "prb_smart@hotmail.com";
    public static final String DEFAULT_DEVICE = "MOBILE";
    public static final String DEFAULT_APPLICATION = "STAT";
    public static final String PATTERN_5_ZEROS = "00000";
    public static final String PATTERN_PERCENT_S = "%s";
    public static final String APPLICATION = "application";
    public static final String TRANSACTION_ID = "transactionId";
    public static final String DEVICE_ID = "deviceId";
    public static final String REQUEST_LOG_CONTEXT = "requestLogContext";
    public static final String AUTHENTICATION_PREFIX = "authentication";
    public static final String LOCAL = "local";
    public static final String BUCKETIZER_APP = "BucketizerUtils";
    public static final String SPARK_MASTER = "spark.master";
    public static final String BUCKET_INPUT_FIELD = "value";
    public static final String BUCKET_OUTPUT_FIELD = "bucket";
    public static final String MOCK_DATA_PATH = "classpath:mock/MOCK_DATA_TU.csv";
}
