package com.company.stats.commons;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import java.util.Map;

/**
 *
 */
@PropertySources({
        @PropertySource(value = Constants.APPLICATION_ERROR_MAPPING_CLASSPATH_PATH, ignoreResourceNotFound = true),
        @PropertySource(value = Constants.APPLICATION_ERROR_MAPPING_RESOURCE_PATH, ignoreResourceNotFound = true)
})
@EnableConfigurationProperties
@ConfigurationProperties(prefix = ErrorConstants.VALIDATION_ERROR_PROPERTIES_ERROR_PREFIX)
public class ValidationErrorMappingProperties {

    private Map<String, String> errors;

    /**
     * @return
     */
    public Map<String, String> getErrors() {
        return errors;
    }

    /**
     * @param errors
     */
    public void setErrors(Map<String, String> errors) {
        this.errors = errors;
    }
}
