package com.company.stats.commons;

import com.company.stats.core.Context;
import com.company.stats.core.LoggerContext;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

/**
 * Common Utils.
 */
public class CommonUtils {

    private static final Logger logger = LoggerFactory.getLogger(CommonUtils.class);

    /**
     * Transform from object to valueType.
     *
     * @param from      from
     * @param valueType valueType
     * @param <T>       Template Object
     * @return Template Object
     * @throws Exception Exception
     */
    public static <T> T transform(Object from, Class<T> valueType) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        String json = objectMapper.writeValueAsString(from);
        return objectMapper.readValue(json, valueType);
    }

    /**
     * Transform from Collection object to valueType Collection.
     *
     * @param from      Objecr
     * @param valueType TypeReference
     * @param <T>       valueType
     * @return valueType
     * @throws Exception
     */
    public static <T> T transformCollection(Object from, TypeReference<T> valueType) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        String json = objectMapper.writeValueAsString(from);
        return objectMapper.readValue(json, valueType);
    }

    /**
     * @param from
     * @param valueType
     * @param <T>
     * @return
     * @throws Exception
     */
    public static <T> String transformToJson(Object from, Class<T> valueType) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        return objectMapper.writeValueAsString(from);
    }

    /**
     * @param from
     * @param valueType
     * @param <T>
     * @return
     * @throws Exception
     */
    public static <T> T transformFromJson(String from, Class<T> valueType) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        return objectMapper.readValue(from, valueType);
    }

    /**
     * Wrap Transformer for Exception Handling.
     *
     * @param object Object
     * @param type   Type
     * @param <T>    Type
     * @return type Class Type
     */
    public static <T> T wrapTransform(Object object, Class<T> type) {
        try {
            return transform(object, type);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * Wrap Transformer Collection for Exception Handling.
     *
     * @param from      Object
     * @param valueType Type
     * @param <T>       Type
     * @return type TypeReference
     */
    public static <T> T wrapTransformCollection(Object from, TypeReference<T> valueType) {
        try {
            return transformCollection(from, valueType);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Get Context with Request Header
     *
     * @param httpServletRequest HttpServletRequest
     * @return context Context
     */
    public static Context getContext(HttpServletRequest httpServletRequest) {
        Context context = new Context();
        if (httpServletRequest != null) {
            context.setTransactionId(httpServletRequest.getHeader(Constants.TRANSACTION_ID));
            if (StringUtils.isNotEmpty(httpServletRequest.getHeader(Constants.DEVICE_ID))) {
                context.setDeviceId(httpServletRequest.getHeader(Constants.DEVICE_ID));
            } else {
                context.setDeviceId(Constants.DEFAULT_DEVICE);
            }
            if (StringUtils.isNotEmpty(httpServletRequest.getHeader(Constants.APPLICATION))) {
                context.setApplication(httpServletRequest.getHeader(Constants.APPLICATION));
            } else {
                context.setApplication(Constants.DEFAULT_APPLICATION);
            }
        }
        return context;
    }

    /**
     * This method is used to load logger context.
     *
     * @param httpServletRequest HttpServletRequest
     * @return loggerContext  LoggerContext
     */
    public static LoggerContext loadLoggerContext(HttpServletRequest httpServletRequest) {
        LoggerContext loggerContext = new LoggerContext();
        if (httpServletRequest != null) {
            Context context =
                    httpServletRequest != null && httpServletRequest.getAttribute(Constants.REQUEST_LOG_CONTEXT) != null ?
                            (Context) httpServletRequest.getAttribute(Constants.REQUEST_LOG_CONTEXT) : new Context();
            loggerContext = CommonUtils.wrapTransform(context, LoggerContext.class);
            loggerContext.setUrl(httpServletRequest.getRequestURL().toString());
            loggerContext.setRequestMethod(httpServletRequest.getMethod());
        }
        return loggerContext;
    }


}
