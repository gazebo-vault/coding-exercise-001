package com.company.stats.commons;

/**
 * Error Constants
 */
public class ErrorConstants {

    //Application Default Error Code and Prefix
    public static final String VALIDATION_ERROR_PROPERTIES_ERROR_PREFIX = "validation";
    public static final String ERROR_MESSAGE_PREFIX = "error";

    public static final String INVALID_LOGIN_CREDENTIALS_ERROR_CODE = "401";
    public static final String FORBIDDEN_ERROR_CODE = "403";
    public static final String UNKNOWN_ERROR_CODE = "500";
    public static final String INPUT_REQUEST_ERROR_CODE = "STAT400";
    public static final String DATA_PROCESSING_ERROR_CODE = "STAT100";
    public static final String EMPTY_RESULT_ERROR_CODE = "STAT101";
    public static final String NO_USER_DATA_BUSINESS_ERROR = "STAT102";

    public static final String NO_USER_DATA_BUSINESS_ERROR_MSG = "No user data available.";

}
