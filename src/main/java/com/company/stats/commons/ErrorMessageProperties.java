package com.company.stats.commons;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import java.util.Map;
import java.util.Objects;

/**
 *
 */
@PropertySources({
        @PropertySource(value = Constants.APPLICATION_MESSAGE_CLASSPATH_PATH, ignoreResourceNotFound = true),
        @PropertySource(value = Constants.APPLICATION_MESSAGE_RESOURCE_PATH, ignoreResourceNotFound = true)
})
@EnableConfigurationProperties
@ConfigurationProperties(prefix = ErrorConstants.ERROR_MESSAGE_PREFIX)
public class ErrorMessageProperties {

    private Map<String, String> errorMsg;

    /**
     * @param key
     * @return
     */
    public String getMessage(String key) {
        String value = null;
        if (Objects.nonNull(errorMsg)) {
            value = errorMsg.get(key);
            if (StringUtils.isEmpty(value)) {
                value = key;
            }
        }
        return value;
    }

    /**
     * @return
     */
    public Map<String, String> getErrorMsg() {
        return errorMsg;
    }

    /**
     * @param errorMsg
     */
    public void setErrorMsg(Map<String, String> errorMsg) {
        this.errorMsg = errorMsg;
    }
}
