package com.company.stats;


import com.company.stats.commons.Constants;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

/**
 *
 */
@Configuration
@PropertySources({
        @PropertySource(value = Constants.APPLICATION_API_CLIENT_CLASSPATH_PATH, ignoreResourceNotFound = true),
        @PropertySource(value = Constants.APPLICATION_API_CLIENT_ENV_CLASSPATH_PATH, ignoreResourceNotFound = true),
        @PropertySource(value = Constants.APPLICATION_DEFAULT_CLASSPATH_PATH, ignoreResourceNotFound = true),
        @PropertySource(value = Constants.APPLICATION_ENV_CLASSPATH_PATH, ignoreResourceNotFound = true),
        @PropertySource(value = Constants.APPLICATION_DEFAULT_RESOURCE_PATH, ignoreResourceNotFound = true),
        @PropertySource(value = Constants.APPLICATION_ENV_RESOURCE_PATH, ignoreResourceNotFound = true),
        @PropertySource(value = Constants.APPLICATION_LOCAL_CLASSPATH_PATH, ignoreResourceNotFound = true)
})
public class EnvironmentPropertyConfig {

}
