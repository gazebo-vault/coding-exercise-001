package com.company.stats.data.helper;

import com.company.stats.commons.ErrorConstants;
import com.company.stats.data.dto.UserDetail;
import com.company.stats.exception.DataProcessingException;
import com.company.stats.service.workflow.helper.BaseDataHelper;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 */
@Component
public class UserDetailsMockDataHelper extends BaseDataHelper {

    private static final Logger logger = LoggerFactory.getLogger(UserDetailsMockDataHelper.class);

    @Value("${cvs.index.user.first}")
    private int firstNameColumnIndex;

    @Value("${cvs.index.user.last}")
    private int lastNameColumnIndex;

    @Value("${cvs.index.user.email}")
    private int emailColumnIndex;

    @Value("${cvs.index.user.age}")
    private int ageColumnIndex;

    @Value("${cvs.index.user.vscore}")
    private int vantageScoreColumnIndex;

    @Value("${cvs.index.user.fscore}")
    private int ficoScoreColumnIndex;

    /**
     * @param inputStream
     * @return
     * @throws Exception
     */
    public List<UserDetail> processUserDetails(InputStream inputStream) throws Exception {
        List<UserDetail> userDetailList = new ArrayList<>();
        if (Objects.nonNull(inputStream)) {
            CSVParser parser = new CSVParser(new InputStreamReader(inputStream), CSVFormat.DEFAULT);
            List<CSVRecord> list = parser.getRecords();
            list.remove(0);
            int columnIndex;
            for (CSVRecord record : list) {
                UserDetail userDetail = new UserDetail();
                columnIndex = 0;
                for (String str : record) {
                    if (columnIndex == firstNameColumnIndex) {
                        userDetail.setFirstName(str);
                    } else if (columnIndex == lastNameColumnIndex) {
                        userDetail.setLastName(str);
                    } else if (columnIndex == emailColumnIndex) {
                        userDetail.setEmail(str);
                    } else if (columnIndex == ageColumnIndex) {
                        userDetail.setAge(Integer.valueOf(str));
                    } else if (columnIndex == vantageScoreColumnIndex) {
                        userDetail.setVantageScore(Integer.valueOf(str));
                    } else if (columnIndex == ficoScoreColumnIndex) {
                        userDetail.setFicoScore(Integer.valueOf(str));
                    }
                    columnIndex++;
                }
                if (StringUtils.isNotBlank(userDetail.getFirstName())) {
                    userDetailList.add(userDetail);
                }

            }
            parser.close();
            inputStream.close();
        } else {
            logger.error(ErrorConstants.NO_USER_DATA_BUSINESS_ERROR_MSG);
            throw new DataProcessingException(ErrorConstants.NO_USER_DATA_BUSINESS_ERROR);
        }
        return userDetailList;
    }

}
