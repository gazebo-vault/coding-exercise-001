package com.company.stats.data.dao.impl;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.lang.reflect.Method;

/**
 * DaoMethod Around Advisor Class
 */
@Aspect
@Component
public class DaoMethodAroundAdvisor {

    private static final Logger logger = LoggerFactory.getLogger(DaoMethodAroundAdvisor.class);

    /**
     * Around Advice.
     *
     * @param joinPoint JoinPoint
     * @return Object
     * @throws Throwable Throwable
     */
    @Around("@annotation(com.company.stats.core.DaoMethod) && execution(* * (..))")
    public Object aroundAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
        Object result;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            result = joinPoint.proceed();
        } catch (Exception e) {
            throw e;
        } finally {
            stopWatch.stop();
            MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
            Method method = methodSignature.getMethod();
            logger.trace(method.getDeclaringClass() + "." + method.getName() + "  Elapsed Time : " + stopWatch
                    .getTotalTimeMillis());
        }
        return result;
    }

}
