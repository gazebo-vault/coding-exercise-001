package com.company.stats.data.dao;

import com.company.stats.data.dto.UserDetail;

import java.util.List;

/**
 *
 */
public interface UserDetailsDao {

    /**
     * @return
     * @throws Exception
     */
    List<UserDetail> getUserDetails() throws Exception;

}
