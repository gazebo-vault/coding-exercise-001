package com.company.stats.data.dao.impl;

import com.company.stats.commons.Constants;
import com.company.stats.commons.cache.AppCacheKeyGenerator;
import com.company.stats.commons.cache.CacheConstants;
import com.company.stats.core.DaoMethod;
import com.company.stats.data.dao.UserDetailsDao;
import com.company.stats.data.dto.UserDetail;
import com.company.stats.data.helper.UserDetailsMockDataHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.cache.annotation.CacheDefaults;
import javax.cache.annotation.CacheResult;
import java.util.List;
import java.util.Objects;

/**
 *
 */
@CacheDefaults(cacheName = CacheConstants.CACHE_NAME)
@Component
public class UserDetailsDaoImpl implements UserDetailsDao {

    @Value(Constants.MOCK_DATA_PATH)
    private Resource resourceFile;

    @Autowired
    private UserDetailsMockDataHelper userDetailsMockDataHelper;

    /**
     *
     * @return
     * @throws Exception
     */
    @CacheResult(cacheKeyGenerator = AppCacheKeyGenerator.class)
    @DaoMethod
    @Override
    public List<UserDetail> getUserDetails() throws Exception {
        return Objects.nonNull(resourceFile) ? userDetailsMockDataHelper.processUserDetails(
                resourceFile.getInputStream()) : null;
    }

}




