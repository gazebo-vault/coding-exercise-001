package com.company.stats.data.dto;

public class UserDetail {

    private String firstName;
    private String lastName;
    private String email;
    private Integer age;
    private Integer vantageScore;
    private Integer ficoScore;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getVantageScore() {
        return vantageScore;
    }

    public void setVantageScore(Integer vantageScore) {
        this.vantageScore = vantageScore;
    }

    public Integer getFicoScore() {
        return ficoScore;
    }

    public void setFicoScore(Integer ficoScore) {
        this.ficoScore = ficoScore;
    }

    @Override
    public String toString() {
        return "UserDetail{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                ", vantageScore=" + vantageScore +
                ", ficoScore=" + ficoScore +
                '}';
    }
}
