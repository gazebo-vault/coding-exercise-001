package com.company.stats;

import com.company.stats.commons.ErrorConstants;
import com.company.stats.commons.ErrorMessageProperties;
import com.company.stats.commons.ValidationErrorMappingProperties;
import com.company.stats.core.BaseResponse;
import com.company.stats.core.Error;
import com.company.stats.core.MoreInfo;
import com.company.stats.exception.DataProcessingException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 */
@Component
public class MessageHandler {

    @Autowired
    private ErrorMessageProperties errorMessageProperties;

    @Autowired
    private ValidationErrorMappingProperties validationMappingProperties;

    /**
     * @param code
     * @return
     * @throws Exception
     */
    public String get(String code) throws Exception {
        return errorMessageProperties.getMessage(code);
    }

    /**
     * @param errorCode
     * @return
     */
    public String getValidationErrorCode(String errorCode) {
        errorCode = StringUtils.trim(errorCode);
        if (validationMappingProperties.getErrors() != null && validationMappingProperties
                .getErrors().containsKey(errorCode)) {
            errorCode = validationMappingProperties.getErrors().get(errorCode);
        }
        return errorCode;
    }

    /**
     * @param errorCode
     * @param exception
     * @return
     */
    public BaseResponse getBaseError(String errorCode, Exception exception) {
        BaseResponse baseResponse = new BaseResponse();
        Error error = new Error();
        try {
            errorCode = StringUtils.trim(getValidationErrorCode(errorCode));
            String msg = StringUtils.isNotBlank(errorCode) ? get(errorCode) : null;
            DataProcessingException dataProcessingException;
            if (exception instanceof DataProcessingException) {
                dataProcessingException = (DataProcessingException) exception;
                if (StringUtils.isNotBlank(dataProcessingException.getErrorMessage())) {
                    msg = dataProcessingException.getErrorMessage();
                }
            }
            msg = StringUtils.isNotBlank(msg) ? msg : get(ErrorConstants.UNKNOWN_ERROR_CODE);
            error.setCode(errorCode);
            error.setMessage(msg);
            if (exception instanceof MethodArgumentNotValidException) {
                List<FieldError> fieldErrors = ((MethodArgumentNotValidException) exception)
                        .getBindingResult().getFieldErrors();
                List<MoreInfo> moreInfos = new ArrayList<>();
                for (FieldError fieldError : fieldErrors) {
                    MoreInfo moreInfo = new MoreInfo();
                    moreInfo.setCode(fieldError.getCode());
                    String errorMessage = fieldError.getDefaultMessage() + " Rejected Value : " + fieldError
                            .getRejectedValue();
                    moreInfo.setMessage(errorMessage);
                    moreInfo.setField(fieldError.getField());
                    moreInfo.setFieldValue(String.valueOf(fieldError.getRejectedValue()));

                    moreInfos.add(moreInfo);
                }
                error.setMoreInfo(moreInfos);
            } else if (Objects.nonNull(exception.getCause())) {
                error.setDeveloperMessage(exception.getCause().getMessage());
            } else {
                error.setDeveloperMessage(exception.getMessage());
            }
        } catch (Exception excp) {
            error.setCode(ErrorConstants.UNKNOWN_ERROR_CODE);
            error.setMessage(exception.getMessage());
        }
        baseResponse.setError(error);
        return baseResponse;
    }


}
