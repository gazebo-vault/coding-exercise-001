package com.company.stats;

import com.company.stats.commons.ErrorConstants;
import com.company.stats.core.BaseResponse;
import com.company.stats.exception.AuthorizationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 *
 */
@RestController
public class ApplicationErrorController implements ErrorController {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationErrorController.class);

    public static final String APPLICATION_SERVLET_ERROR_STATUS_CODE = "javax.servlet.error.status_code";
    public static final String APPLICATION_SERVLET_ERROR_EXCEPTION = "javax.servlet.error.exception";
    private static final String PATH = "/error";

    @Autowired
    private MessageHandler messageHandler;

    /**
     * @return
     */
    @Override
    public String getErrorPath() {
        return PATH;
    }

    /**
     *
     * @param request
     * @return
     */
    @RequestMapping(path = "/error", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity handleError(HttpServletRequest request) {
        ResponseEntity responseEntity = new ResponseEntity(HttpStatus.HTTP_VERSION_NOT_SUPPORTED);
        Exception exception = (Exception) request.getAttribute(APPLICATION_SERVLET_ERROR_EXCEPTION);
        if (Objects.nonNull(exception) && Objects.nonNull(exception.getCause())
                && exception.getCause() instanceof AuthorizationException) {
            AuthorizationException authorizationException = (AuthorizationException) exception.getCause();
            if (ErrorConstants.INVALID_LOGIN_CREDENTIALS_ERROR_CODE
                    .equalsIgnoreCase(authorizationException.getErrorCode())) {
                BaseResponse baseResponse = messageHandler.getBaseError(
                        ErrorConstants.INVALID_LOGIN_CREDENTIALS_ERROR_CODE, exception);
                responseEntity = new ResponseEntity(baseResponse, HttpStatus.FORBIDDEN);
            }
        }
        return responseEntity;
    }

}