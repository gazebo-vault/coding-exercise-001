package com.company.stats.service;

import com.company.stats.api.model.BucketDataRs;
import com.company.stats.api.model.NameMatchRs;
import com.company.stats.api.model.StatisticsRs;
import com.company.stats.core.Context;

/**
 *
 */
public interface StatsService {

    /**
     * @param context
     * @return
     * @throws Exception
     */
    StatisticsRs getStatistics(Context context) throws Exception;

    /**
     * @param columnName
     * @param bucket
     * @param context
     * @return
     * @throws Exception
     */
    BucketDataRs getBucketData(String columnName, Integer bucket, Context context) throws Exception;

    /**
     * @param firstName
     * @param context
     * @return
     * @throws Exception
     */
    NameMatchRs getNameMatch(String firstName, Context context) throws Exception;

}
