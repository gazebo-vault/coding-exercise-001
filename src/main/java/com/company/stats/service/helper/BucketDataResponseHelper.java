package com.company.stats.service.helper;

import com.company.stats.api.model.BucketDataRs;
import com.company.stats.service.workflow.domain.BucketDataContext;
import com.company.stats.core.Context;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class BucketDataResponseHelper extends BaseHelper {

    /**
     *
     * @param bucketDataContext
     * @param context
     * @return
     * @throws Exception
     */
    public BucketDataRs buildResponse(BucketDataContext bucketDataContext, Context context) throws Exception {
        return transform(bucketDataContext, BucketDataRs.class);
    }

}
