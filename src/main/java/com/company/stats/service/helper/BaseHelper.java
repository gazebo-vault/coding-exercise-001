package com.company.stats.service.helper;

import com.company.stats.commons.CommonUtils;

/**
 * {@link BaseHelper} parent for business service helpers.
 */
public class BaseHelper {

    /**
     *
     * @param from
     * @param valueType
     * @param <T>
     * @return
     * @throws Exception
     */
    public  <T> T transform(Object from, Class<T> valueType) throws Exception{
        return CommonUtils.transform(from,valueType);
    }

}
