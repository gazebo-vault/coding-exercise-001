package com.company.stats.service.helper;

import com.company.stats.api.model.StatisticsRs;
import com.company.stats.service.workflow.domain.StatsContext;
import com.company.stats.core.Context;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class StatisticsResponseHelper extends BaseHelper {

    /**
     *
     * @param statsContext
     * @param context
     * @return
     * @throws Exception
     */
    public StatisticsRs buildResponse(StatsContext statsContext, Context context) throws Exception {
        return transform(statsContext, StatisticsRs.class);
    }

}
