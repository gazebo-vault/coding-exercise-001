package com.company.stats.service.helper;

import com.company.stats.api.model.NameMatchRs;
import com.company.stats.core.Context;
import com.company.stats.service.workflow.domain.NameMatchContext;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class NameMatchResponseHelper extends BaseHelper {

    /**
     *
     * @param nameMatchContext
     * @param context
     * @return
     * @throws Exception
     */
    public NameMatchRs buildResponse(NameMatchContext nameMatchContext, Context context) throws Exception {
        return transform(nameMatchContext, NameMatchRs.class);
    }

}
