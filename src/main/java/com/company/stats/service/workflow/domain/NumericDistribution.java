package com.company.stats.service.workflow.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class NumericDistribution {
    private String columnName;
    private Integer count = 0;
    private Integer distinctCount = 0;
    private Integer min = 0;
    private Integer max = 0;
    private Integer sum = 0;
    private BigDecimal mean = new BigDecimal(0);
    private BigDecimal stddev = new BigDecimal(0);
    private List<Integer> values = new ArrayList<>();

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getDistinctCount() {
        return distinctCount;
    }

    public void setDistinctCount(Integer distinctCount) {
        this.distinctCount = distinctCount;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        if (min <= this.min || this.min == 0) {
            this.min = min;
        }
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        if (max >= this.max || this.max == 0) {
            this.max = max;
        }
    }

    public BigDecimal getMean() {
        return mean;
    }

    public void setMean(BigDecimal mean) {
        this.mean = mean;
    }

    public BigDecimal getStddev() {
        return stddev;
    }

    public void setStddev(BigDecimal stddev) {
        this.stddev = stddev;
    }

    public List<Integer> getValues() {
        return values;
    }

    public void setValues(List<Integer> values) {
        this.values = values;
    }

    public void addValue(Integer value) {
        if (!values.contains(value)) {
            distinctCount++;
        }
        count++;
        sum += value;
        values.add(value);
    }

    public void calculateMean() {
        if (values.size() > 0) {
            mean = BigDecimal.valueOf((double) sum / values.size());
        }
    }

    public void calculateSD() {
        if (values.size() > 0) {
            double standardDeviation = 0.0;
            for (Integer num : values) {
                standardDeviation += Math.pow((double) num - mean.doubleValue(), 2);
            }
            stddev = BigDecimal.valueOf(Math.sqrt(standardDeviation / values.size()));
        }
    }
}
