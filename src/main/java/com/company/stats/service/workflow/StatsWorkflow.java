package com.company.stats.service.workflow;

import com.company.stats.core.Context;
import com.company.stats.core.WorkflowMethod;
import com.company.stats.data.dao.UserDetailsDao;
import com.company.stats.service.workflow.domain.BucketDataContext;
import com.company.stats.service.workflow.domain.NameMatchContext;
import com.company.stats.service.workflow.domain.StatsContext;
import com.company.stats.service.workflow.helper.BucketDataHelper;
import com.company.stats.service.workflow.helper.NameMatchDataHelper;
import com.company.stats.service.workflow.helper.StatisticsDataHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class StatsWorkflow extends BaseWorkflow {

    @Autowired
    private StatisticsDataHelper statisticsDataHelper;

    @Autowired
    private BucketDataHelper bucketDataHelper;

    @Autowired
    private NameMatchDataHelper nameMatchDataHelper;

    @Autowired
    private UserDetailsDao userDetailsDao;

    /**
     * @param statsContext
     * @param context
     * @return
     * @throws Exception
     */
    @WorkflowMethod
    public StatsContext getStatistics(StatsContext statsContext, Context context) throws Exception {
        statsContext.setUserDetailList(userDetailsDao.getUserDetails());
        return statisticsDataHelper.processNumericDistribution(statsContext, context);
    }

    /**
     * @param bucketDataContext
     * @param context
     * @return
     * @throws Exception
     */
    @WorkflowMethod
    public BucketDataContext getBucketData(BucketDataContext bucketDataContext, Context context) throws Exception {
        bucketDataContext.setUserDetailList(userDetailsDao.getUserDetails());
        return bucketDataHelper.processBucketDistribution(bucketDataContext, context);
    }

    /**
     * @param nameMatchContext
     * @param context
     * @return
     * @throws Exception
     */
    @WorkflowMethod
    public NameMatchContext getNameMatches(NameMatchContext nameMatchContext, Context context) throws Exception {
        nameMatchContext.setUserDetailList(userDetailsDao.getUserDetails());
        return nameMatchDataHelper.processSoundexNameMatching(nameMatchContext, context);
    }

}
