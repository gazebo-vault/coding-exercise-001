package com.company.stats.service.workflow.helper;

import com.company.stats.commons.CommonUtils;

/**
 * {@link BaseDataHelper} parent for workflow helpers.
 */
public class BaseDataHelper {

    /**
     *
     * @param from
     * @param valueType
     * @param <T>
     * @return
     * @throws Exception
     */
    public  <T> T transform(Object from, Class<T> valueType) throws Exception{
        return CommonUtils.transform(from,valueType);
    }

}
