package com.company.stats.service.workflow.helper;

import com.company.stats.commons.SoundexUtils;
import com.company.stats.core.Context;
import com.company.stats.data.dto.UserDetail;
import com.company.stats.service.workflow.domain.NameMatchContext;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 *
 */
@Component
public class NameMatchDataHelper extends BaseDataHelper {

    /**
     * @param nameMatchContext
     * @param context
     * @return
     * @throws Exception
     */
    public NameMatchContext processSoundexNameMatching(NameMatchContext nameMatchContext, Context context) throws Exception {
        Set<String> matchList = new HashSet<>();
        String queryNameCode = SoundexUtils.getGode(nameMatchContext.getFirstName());
        String firstNameCode;
        if (Objects.nonNull(nameMatchContext) && Objects.nonNull(nameMatchContext.getUserDetailList())
                && !nameMatchContext.getUserDetailList().isEmpty()) {
            for (UserDetail userDetail : nameMatchContext.getUserDetailList()) {
                if (!userDetail.getFirstName().equalsIgnoreCase(nameMatchContext.getFirstName())) {
                    firstNameCode = SoundexUtils.getGode(userDetail.getFirstName());
                    if (firstNameCode.equalsIgnoreCase(queryNameCode)) {
                        matchList.add(userDetail.getFirstName());
                    }
                }
            }
            nameMatchContext.setOutput(matchList);
        }
        return nameMatchContext;
    }
}
