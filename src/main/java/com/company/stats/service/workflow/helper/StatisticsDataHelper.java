package com.company.stats.service.workflow.helper;

import com.company.stats.core.Context;
import com.company.stats.data.dto.UserDetail;
import com.company.stats.service.workflow.domain.NumericDistribution;
import com.company.stats.service.workflow.domain.Statistics;
import com.company.stats.service.workflow.domain.StatsContext;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 *
 */
@Component
public class StatisticsDataHelper extends BaseDataHelper {

    /**
     * @param statsContext
     * @param context
     * @return
     * @throws Exception
     */
    public StatsContext processNumericDistribution(StatsContext statsContext, Context context) throws Exception {
        if (Objects.nonNull(statsContext) && Objects.nonNull(statsContext.getUserDetailList())
                && !statsContext.getUserDetailList().isEmpty()) {
            NumericDistribution ageDistribution = new NumericDistribution();
            ageDistribution.setColumnName("age");
            NumericDistribution vScoreDistribution = new NumericDistribution();
            vScoreDistribution.setColumnName("vantageScore");
            NumericDistribution fScoreDistribution = new NumericDistribution();
            fScoreDistribution.setColumnName("ficoScore");
            for (UserDetail userDetail : statsContext.getUserDetailList()) {
                processDistributionAttributes(ageDistribution, userDetail.getAge());
                processDistributionAttributes(vScoreDistribution, userDetail.getVantageScore());
                processDistributionAttributes(fScoreDistribution, userDetail.getFicoScore());
            }
            processDistribution(ageDistribution);
            processDistribution(vScoreDistribution);
            processDistribution(fScoreDistribution);
            statsContext.setStatistics(new Statistics());
            statsContext.getStatistics().getNumericDistributions().add(ageDistribution);
            statsContext.getStatistics().getNumericDistributions().add(vScoreDistribution);
            statsContext.getStatistics().getNumericDistributions().add(fScoreDistribution);
        }
        return statsContext;
    }

    /**
     * @param distribution
     * @param value
     */
    private void processDistributionAttributes(NumericDistribution distribution, Integer value) {
        distribution.setMin(value);
        distribution.setMax(value);
        distribution.addValue(value);
    }

    /**
     * @param distribution
     */
    private void processDistribution(NumericDistribution distribution) {
        distribution.calculateMean();
        distribution.calculateSD();
    }

}
