package com.company.stats.service.workflow.domain;

import java.util.List;

public class BaseContext {
    protected Error error;
    protected List<Warning> warnings;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public List<Warning> getWarnings() {
        return warnings;
    }

    public void setWarnings(List<Warning> warnings) {
        this.warnings = warnings;
    }
}
