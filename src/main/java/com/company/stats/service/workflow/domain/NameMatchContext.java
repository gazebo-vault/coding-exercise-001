package com.company.stats.service.workflow.domain;

import com.company.stats.data.dto.UserDetail;

import java.util.List;
import java.util.Set;

public class NameMatchContext extends BaseContext {

    private List<UserDetail> userDetailList;
    private String firstName;
    private Set<String> output;

    public List<UserDetail> getUserDetailList() {
        return userDetailList;
    }

    public void setUserDetailList(List<UserDetail> userDetailList) {
        this.userDetailList = userDetailList;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Set<String> getOutput() {
        return output;
    }

    public void setOutput(Set<String> output) {
        this.output = output;
    }
}
