package com.company.stats.service.workflow.domain;

import com.company.stats.commons.ColumnNameEnum;
import com.company.stats.data.dto.UserDetail;

import java.util.List;

public class BucketDataContext extends BaseContext {

    private List<UserDetail> userDetailList;
    private ColumnNameEnum columnName;
    private Integer bucket;
    private List<BucketData> bucketData;

    public List<UserDetail> getUserDetailList() {
        return userDetailList;
    }

    public void setUserDetailList(List<UserDetail> userDetailList) {
        this.userDetailList = userDetailList;
    }

    public Integer getBucket() {
        return bucket;
    }

    public void setBucket(Integer bucket) {
        this.bucket = bucket;
    }

    public ColumnNameEnum getColumnName() {
        return columnName;
    }

    public void setColumnName(ColumnNameEnum columnName) {
        this.columnName = columnName;
    }

    public List<BucketData> getBucketData() {
        return bucketData;
    }

    public void setBucketData(List<BucketData> bucketData) {
        this.bucketData = bucketData;
    }
}
