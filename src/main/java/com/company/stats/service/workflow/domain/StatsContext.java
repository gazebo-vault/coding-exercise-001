package com.company.stats.service.workflow.domain;

import com.company.stats.data.dto.UserDetail;

import java.util.List;

public class StatsContext extends BaseContext {

    private List<UserDetail> userDetailList;
    private Statistics statistics;

    public List<UserDetail> getUserDetailList() {
        return userDetailList;
    }

    public void setUserDetailList(List<UserDetail> userDetailList) {
        this.userDetailList = userDetailList;
    }

    public Statistics getStatistics() {
        return statistics;
    }

    public void setStatistics(Statistics statistics) {
        this.statistics = statistics;
    }
}
