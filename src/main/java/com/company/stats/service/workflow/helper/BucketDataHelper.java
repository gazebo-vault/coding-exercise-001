package com.company.stats.service.workflow.helper;

import com.company.stats.commons.BucketizerUtils;
import com.company.stats.commons.ColumnNameEnum;
import com.company.stats.core.Context;
import com.company.stats.data.dto.UserDetail;
import com.company.stats.service.workflow.domain.BucketData;
import com.company.stats.service.workflow.domain.BucketDataContext;
import io.micrometer.core.instrument.util.StringUtils;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 *
 */
@Component
public class BucketDataHelper extends BaseDataHelper {

    private static final Logger logger = LoggerFactory.getLogger(BucketDataHelper.class);

    /**
     * @param bucketDataContext
     * @param context
     * @return
     * @throws Exception
     */
    public BucketDataContext processBucketDistribution(BucketDataContext bucketDataContext, Context context) throws Exception {
        List<BucketData> bucketDataList = new ArrayList<>();
        if (Objects.nonNull(bucketDataContext) && Objects.nonNull(bucketDataContext.getUserDetailList())
                && !bucketDataContext.getUserDetailList().isEmpty()) {
            if (Objects.isNull(bucketDataContext.getBucket())) {
                bucketDataContext.setBucket(5);
            }
            List<Double> valueList = new ArrayList<>();
            for (UserDetail userDetail : bucketDataContext.getUserDetailList()) {
                valueList.add(assignColumnValue(bucketDataContext.getColumnName(), userDetail));
            }
            Collections.sort(valueList);
            List<Row> result = BucketizerUtils.bucketize(valueList, bucketDataContext.getBucket());
            Object[] fields;
            int startIndex = 0;
            int endIndex;
            BucketData bucketData = null;
            for (Row row : result) {
                logger.info(row.json());
                fields = ((GenericRowWithSchema) row).values();
                if (!(Objects.nonNull(bucketData)
                        && StringUtils.isNotBlank(bucketData.getBucketNumber())
                        && bucketData.getBucketNumber().equalsIgnoreCase(String.valueOf(((Double) fields[1]).intValue() + 1)))) {
                    bucketData = new BucketData();
                    bucketDataList.add(bucketData);
                    bucketData.setBucketNumber(String.valueOf(((Double) fields[1]).intValue() + 1));
                    startIndex = ((Double) fields[0]).intValue();
                }
                bucketData.incrementCount();
                endIndex = ((Double) fields[0]).intValue();
                bucketData.setRange(startIndex + " - " + endIndex);
            }
            bucketDataContext.setBucketData(bucketDataList);
        }
        return bucketDataContext;
    }

    /**
     * @param columnName
     * @param userDetail
     * @return
     */
    private Double assignColumnValue(ColumnNameEnum columnName, UserDetail userDetail) {
        Double val = null;
        switch (columnName) {
            case AGE:
                val = Double.valueOf(userDetail.getAge());
                break;
            case VANTAGE_SORE:
                val = Double.valueOf(userDetail.getVantageScore());
                break;
            case FICO_SCORE:
                val = Double.valueOf(userDetail.getFicoScore());
                break;
            default:
        }
        return val;
    }

}
