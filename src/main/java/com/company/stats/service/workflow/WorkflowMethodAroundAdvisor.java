package com.company.stats.service.workflow;

import com.company.stats.commons.ErrorConstants;
import com.company.stats.exception.DataProcessingException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.naming.NamingException;
import java.lang.reflect.Method;
import java.security.InvalidParameterException;
import java.sql.SQLException;

/**
 * WorkflowMethod Around Advisor Class
 */
@Aspect
@Component
public class WorkflowMethodAroundAdvisor {

    private static final Logger logger = LoggerFactory.getLogger(WorkflowMethodAroundAdvisor.class);

    /**
     * Around Advice.
     *
     * @param joinPoint JoinPoint
     * @return Object
     * @throws Throwable Throwable
     */
    @Around("@annotation(com.company.stats.core.WorkflowMethod) && execution(* * (..))")
    public Object aroundAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
        Object result;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            result = joinPoint.proceed();
        } catch (SQLException | NamingException | InvalidParameterException e) {
            throw new DataProcessingException(e, ErrorConstants.DATA_PROCESSING_ERROR_CODE);
        } catch (Exception e) {
            throw e;
        } finally {
            stopWatch.stop();
            MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
            Method method = methodSignature.getMethod();
            logger.info(method.getDeclaringClass() + "." + method.getName() + "  Elapsed Time : " + stopWatch
                    .getTotalTimeMillis());
        }
        return result;
    }

}
