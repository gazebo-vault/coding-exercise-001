package com.company.stats.service.workflow.domain;

public class BucketData {

    private String bucketNumber;
    private String range;
    private Integer count = 0;

    public String getBucketNumber() {
        return bucketNumber;
    }

    public void setBucketNumber(String bucketNumber) {
        this.bucketNumber = bucketNumber;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public void incrementCount() {
        this.count++;
    }
}
