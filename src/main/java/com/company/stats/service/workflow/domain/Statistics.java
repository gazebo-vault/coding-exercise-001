package com.company.stats.service.workflow.domain;

import java.util.ArrayList;
import java.util.List;

public class Statistics {

    private List<NumericDistribution> numericDistributions = new ArrayList<>();

    public List<NumericDistribution> getNumericDistributions() {
        return numericDistributions;
    }

    public void setNumericDistributions(List<NumericDistribution> numericDistributions) {
        this.numericDistributions = numericDistributions;
    }
}
