package com.company.stats.service.impl;

import com.company.stats.api.model.BucketDataRs;
import com.company.stats.api.model.NameMatchRs;
import com.company.stats.api.model.StatisticsRs;
import com.company.stats.commons.ColumnNameEnum;
import com.company.stats.core.Context;
import com.company.stats.service.StatsService;
import com.company.stats.service.helper.BucketDataResponseHelper;
import com.company.stats.service.helper.NameMatchResponseHelper;
import com.company.stats.service.helper.StatisticsResponseHelper;
import com.company.stats.service.workflow.StatsWorkflow;
import com.company.stats.service.workflow.domain.BucketDataContext;
import com.company.stats.service.workflow.domain.NameMatchContext;
import com.company.stats.service.workflow.domain.StatsContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class StatsServiceImpl extends BaseServiceImpl implements StatsService {

    @Autowired
    private StatsWorkflow statsWorkflow;

    @Autowired
    private StatisticsResponseHelper statisticsResponseHelper;

    @Autowired
    private BucketDataResponseHelper bucketDataResponseHelper;

    @Autowired
    private NameMatchResponseHelper nameMatchResponseHelper;

    /**
     * @param context
     * @return
     * @throws Exception
     */
    @Override
    public StatisticsRs getStatistics(Context context) throws Exception {
        StatsContext statsContext = new StatsContext();
        return checkStandardErrors(statisticsResponseHelper.buildResponse(
                statsWorkflow.getStatistics(statsContext, context), context));
    }

    /**
     * @param columnName
     * @param bucket
     * @param context
     * @return
     * @throws Exception
     */
    @Override
    public BucketDataRs getBucketData(String columnName, Integer bucket, Context context) throws Exception {
        BucketDataContext bucketDataContext = new BucketDataContext();
        bucketDataContext.setBucket(bucket);
        bucketDataContext.setColumnName(ColumnNameEnum.lookupByName(columnName));
        return checkStandardErrors(bucketDataResponseHelper.buildResponse(statsWorkflow.getBucketData(
                bucketDataContext, context), context));
    }

    /**
     * @param firstName
     * @param context
     * @return
     * @throws Exception
     */
    @Override
    public NameMatchRs getNameMatch(String firstName, Context context) throws Exception {
        NameMatchContext nameMatchContext = new NameMatchContext();
        nameMatchContext.setFirstName(firstName);
        return checkStandardErrors(nameMatchResponseHelper.buildResponse(statsWorkflow.getNameMatches(
                nameMatchContext, context), context));
    }
}
