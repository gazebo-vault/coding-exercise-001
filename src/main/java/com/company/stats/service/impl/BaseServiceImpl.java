package com.company.stats.service.impl;

import com.company.stats.api.model.BucketDataRs;
import com.company.stats.api.model.NameMatchRs;
import com.company.stats.api.model.StatisticsRs;
import com.company.stats.commons.ErrorConstants;
import com.company.stats.exception.DataProcessingException;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 *
 */
public class BaseServiceImpl {

    public static final String STRING_EMPTY_RESULT = "Empty result.";

    /**
     * Check Standard Exceptions
     *
     * @param result
     * @param <T>
     * @return
     */
    public <T> T checkStandardErrors(T result) {
        if (result instanceof StatisticsRs
                || result instanceof BucketDataRs
                || result instanceof NameMatchRs) {
            checkEmptyResult(result);
        }
        return result;
    }

    /**
     * Check empty result
     *
     * @param data
     * @param <T>
     */
    public <T> void checkEmptyResult(T data) {
        Boolean emptyData = false;
        if (Objects.isNull(data)) {
            emptyData = true;
        } else {
            if (data instanceof List && ((List) data).isEmpty()) {
                emptyData = true;
            } else if (data instanceof Map && ((Map) data).isEmpty()) {
                emptyData = true;
            } else if (data instanceof String && StringUtils.isEmpty((String) data)) {
                emptyData = true;
            }
        }
        if (emptyData) {
            throw new DataProcessingException(ErrorConstants.EMPTY_RESULT_ERROR_CODE);
        }
    }

}
