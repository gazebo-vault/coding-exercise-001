package com.company.stats.logging;

import com.company.stats.api.controller.BaseController;
import com.company.stats.commons.CommonUtils;
import com.company.stats.commons.Constants;
import com.company.stats.core.BaseRequest;
import com.company.stats.core.LoggerContext;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * Application Logging Aspect
 */
@Component
@Aspect
public class ApplicationLoggingAspect {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationLoggingAspect.class);

    /**
     * Log the controller objects' method calls
     */
    @Pointcut("execution(* com.company.stats.api.controller..*.*(..))")
    public void logControllerMethods() {
        // intentionally empty
    }

    /**
     * Log the services objects' method calls
     */
    @Pointcut("execution(* com.company.stats.service.impl..*.*(..))")
    public void logServiceMethods() {
        // intentionally empty
    }

    /**
     * Log the workflow objects' method calls
     */
    @Pointcut("execution(* com.company.stats.service.workflow..*.*(..))")
    public void logWorkflowMethods() {
        // intentionally empty
    }

    /**
     * Perform the logging via the logging library used - in this case log4j2
     *
     * @param proceedingJoinPoint - the proxy class reference
     * @return - returns the response from the wrapped class by the AOP proxy
     * @throws Throwable - any generic exception that is not caught by the
     *                   {@link AfterThrowing}
     */
    @Around("logWorkflowMethods()")
    public Object logWorkflowMethods(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Object response;
        Object target = proceedingJoinPoint.getTarget();
        String className = target.getClass().getSimpleName();
        String methodName = proceedingJoinPoint.getSignature().getName();
        logger.trace("{} >> {}", className, methodName);
        response = proceedingJoinPoint.proceed();
        logger.trace("{} << {}", className, methodName);
        return response;
    }

    /**
     * Perform the logging via the logging library used - in this case log4j2
     *
     * @param proceedingJoinPoint - the proxy class reference
     * @return - returns the response from the wrapped class by the AOP proxy
     * @throws Throwable - any generic exception that is not caught by the
     *                   {@link AfterThrowing}
     */
    @Around("logServiceMethods()")
    public Object logServiceMethods(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Object response;
        Object target = proceedingJoinPoint.getTarget();
        String className = target.getClass().getSimpleName();
        String methodName = proceedingJoinPoint.getSignature().getName();
        logger.trace("{} >> {}", className, methodName);
        response = proceedingJoinPoint.proceed();
        logger.trace("{} << {}", className, methodName);
        return response;
    }

    /**
     * Perform the logging via the logging library used - in this case log4j2
     *
     * @param proceedingJoinPoint - the proxy class reference
     * @return - returns the response from the wrapped class by the AOP proxy
     * @throws Throwable - any generic exception that is not caught by the
     *                   {@link AfterThrowing}
     */
    @Around(" logControllerMethods()")
    public Object logControllerMethods(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Object response;
        Object target = proceedingJoinPoint.getTarget();
        String className = target.getClass().getSimpleName();
        String methodName = proceedingJoinPoint.getSignature().getName();
        logger.trace("{} >> {}", className, methodName);
        String message = getMessage(proceedingJoinPoint);
        try {
            response = proceedingJoinPoint.proceed();
        } catch (Exception excp) {
            logMethodArgument(proceedingJoinPoint.getArgs(), message, true);
            throw excp;
        }
        logMethodArgument(proceedingJoinPoint.getArgs(), message, false);
        logResult(response, message);
        logger.trace("{} << {}", className, methodName);
        if (response instanceof ResponseEntity && target instanceof BaseController) {
            BaseController baseController = (BaseController) target;
            HttpServletRequest httpServletRequest = baseController.getHttpServletRequest();
            LoggerContext loggerContext = CommonUtils.loadLoggerContext(baseController.getHttpServletRequest());
            loggerContext.setUrl(httpServletRequest.getRequestURL().toString());
            loggerContext.setMethodName(methodName);
            loggerContext.setClassName(className);
            ResponseEntity responseEntity = (ResponseEntity) response;
            if (!HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                setRequest(proceedingJoinPoint.getArgs(), loggerContext);
                loggerContext.setApiResponse(response);
                logger.error("", loggerContext);
            }
            httpServletRequest.setAttribute(Constants.LOG_TYPE_ANALYTICS, loggerContext);
        }
        return response;
    }

    /**
     * @param proceedingJoinPoint ProceedingJoinPoint
     * @return object Object
     * @throws Throwable Throwable
     */

    // @Around("logEngineMethods()")
    public Object logMethodsWithParams(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Object response;
        Object target = proceedingJoinPoint.getTarget();
        String className = target.getClass().getSimpleName();
        String methodName = proceedingJoinPoint.getSignature().getName();

        logger.trace("{} >> {}", className, methodName);

        if (proceedingJoinPoint.getArgs() != null && proceedingJoinPoint.getArgs().length > 0) {
            StringBuilder startMessageStringBuffer = new StringBuilder("(");
            Object[] args = proceedingJoinPoint.getArgs();
            for (Object arg : args) {
                startMessageStringBuffer.append(arg).append(",");
            }
            if (args.length > 0) {
                startMessageStringBuffer.deleteCharAt(startMessageStringBuffer.length() - 1);
                startMessageStringBuffer.append(")");
                logger.trace("Input Parameters [" + className + "] [" + methodName + "] " + startMessageStringBuffer
                        .toString());
            }

        }

        response = proceedingJoinPoint.proceed();

        if (response != null) {
            logger.trace("Return Value [" + className + "] [" + methodName + "] " + response.toString());
        }
        logger.trace("{} << {}", className, methodName);
        return response;
    }

    /**
     * @param joinPoint ProceedingJoinPoint
     * @return object Object
     * @throws Throwable Throwable
     */
    @Around("@annotation(LogExecutionTime)")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Object proceed = joinPoint.proceed();
        stopWatch.stop();

        logger.debug(joinPoint.getSignature() + " executed in " + stopWatch.getTotalTimeSeconds() + " seconds");
        return proceed;
    }

    /**
     * Log only the exceptions bubbling up the controller class
     *
     * @param ex Throwable
     */
    @AfterThrowing(pointcut = "logControllerMethods()", throwing = "ex")
    public void logExceptionsFromController(Throwable ex) {
        //logger.error("Error occurred.", ex);
    }

    /**
     * @param objects
     * @param message
     * @param isError
     * @throws Throwable
     */
    public void logMethodArgument(Object[] objects, String message, boolean isError) throws Throwable {
        if (objects != null) {
            for (Object object : objects) {
                String logLine = message + toJson(object);
                log(logLine, isError);
            }
        }
    }

    /**
     * @param objects
     * @param loggerContext
     * @throws Throwable
     */
    public void setRequest(Object[] objects, LoggerContext loggerContext) throws Throwable {
        if (objects != null) {
            for (Object object : objects) {
                if (object instanceof BaseRequest) {
                    loggerContext.setApiRequest(object);
                    break;
                }
            }
        }
    }

    /**
     * @param object
     * @param message
     */
    public void logResult(Object object, String message) {
        if (logger.isDebugEnabled()) {
            if (object != null) {
                message = message + toJson(object);
                log(message, false);
            }
        }
    }

    /**
     * @param msg
     * @param isError
     */
    public void log(String msg, boolean isError) {
        if (isError) {
            logger.error(msg);
        } else {
            logger.debug(msg);
        }
    }

    /**
     * @param object
     * @return
     */
    public String toJson(Object object) {
        StringBuilder sb = new StringBuilder();
        try {
            if (object != null) {
                sb.append(" Type : ").append(object.getClass().getName());
                sb.append(" Value : ").append(CommonUtils.transformToJson(object, object.getClass()));
            }
        } catch (Exception e) {
            logger.error("ApplicationLoggingAspect.toJson()", e);
        }
        return sb.toString();
    }

    /**
     * @param joinPoint
     * @return
     * @throws Exception
     */
    public String getMessage(JoinPoint joinPoint) throws Exception {
        StringBuilder sb = new StringBuilder();
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        sb.append(method.getDeclaringClass()).append(".").append(method.getName()).append(" ");
        return sb.toString();
    }
}
