package com.company.stats.api.controller;

import com.company.stats.api.HealthApi;
import com.company.stats.api.model.HealthStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 */
@RestController
public class HealthApiController extends BaseController implements HealthApi {

    /**
     * @return
     * @throws Exception
     */
    public ResponseEntity<HealthStatus> getHealthStatus() throws Exception {
        HealthStatus healthStatus = new HealthStatus();
        healthStatus.setStatus("UP");
        return ResponseEntity.ok(healthStatus);
    }

}
