package com.company.stats.api.controller;

import com.company.stats.api.model.BucketDataRs;
import com.company.stats.api.model.NameMatchRs;
import com.company.stats.api.model.StatisticsRs;
import com.company.stats.api.validator.NumericColumnValuesAllowed;
import com.company.stats.core.BaseResponse;
import com.company.stats.core.Context;
import com.company.stats.service.StatsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 */
@Api(value = "Statistics Api", tags = {"Statistics"})
@RestController
@Validated
public class StatisticsController extends BaseController {

    @Autowired
    StatsService statsService;

    /**
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "An endpoint which returns the statistics for all the numeric columns from the csv file. The"
            + "statistics includes min, max, count, distinct count, mean and standard deviation.",
            nickname = "getStatistics",
            notes = "", response = StatisticsRs.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = StatisticsRs.class),
            @ApiResponse(code = 400, message = "Bad Request", response = BaseResponse.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = BaseResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = BaseResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = BaseResponse.class),
            @ApiResponse(code = 429, message = "Too Many Requests", response = BaseResponse.class),
            @ApiResponse(code = 500, message = "Internal Error", response = BaseResponse.class),
            @ApiResponse(code = 502, message = "Bad Gateway", response = BaseResponse.class),
            @ApiResponse(code = 503, message = "Service Unavailable", response = BaseResponse.class),
            @ApiResponse(code = 504, message = "Gateway Timeout", response = BaseResponse.class)})
    @GetMapping(value = "/statistics", produces = {"application/json"})
    @PreAuthorize("hasRole(@environment.getProperty('authority.group.admin'))")
    public ResponseEntity<StatisticsRs> getStatistics() throws Exception {
        Context context = getContext();
        return new ResponseEntity(statsService.getStatistics(getContext()), HttpStatus.OK);
    }

    /**
     * @param columnName
     * @param bucket
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "An endpoint which implements Bucketizer. It transforms a column of continuous features to a"
            + "column of feature buckets, where the buckets are specified by users. For this endpoint we will"
            + "default the buckets to 5. This endpoint returns the bucketized data only for numeric columns"
            + "which are passed as a query parameter in the URL. Nonnumeric columns shall be considered as"
            + "bad request.",
            nickname = "getBucketData",
            notes = "", response = BucketDataRs.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = BucketDataRs.class),
            @ApiResponse(code = 400, message = "Bad Request", response = BaseResponse.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = BaseResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = BaseResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = BaseResponse.class),
            @ApiResponse(code = 429, message = "Too Many Requests", response = BaseResponse.class),
            @ApiResponse(code = 500, message = "Internal Error", response = BaseResponse.class),
            @ApiResponse(code = 502, message = "Bad Gateway", response = BaseResponse.class),
            @ApiResponse(code = 503, message = "Service Unavailable", response = BaseResponse.class),
            @ApiResponse(code = 504, message = "Gateway Timeout", response = BaseResponse.class)})
    @GetMapping(value = "/bucketdata", produces = {"application/json"})
    @PreAuthorize("hasRole(@environment.getProperty('authority.group.user'))")
    public ResponseEntity<BucketDataRs> getBucketData(
            @NumericColumnValuesAllowed(propName = "columnName", values = {"age", "vantageScore", "ficoScore"})
            @RequestParam(value = "columnName") String columnName,
            @RequestParam(value = "bucket", required = false) Integer bucket) throws Exception {
        Context context = getContext();
        return new ResponseEntity(statsService.getBucketData(columnName, bucket, getContext()), HttpStatus.OK);
    }

    /**
     * @param firstName
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "An endpoint which implements Soundex phonetic algorithm."
            + "The main principle behind this algorithm is that consonants are grouped depending on the"
            + "ordinal numbers and finally encoded into a value against which others are matched. It aims to"
            + "find a code for every word by above process which is called Soundex code."
            + "The Soundex code for a name consists of a letter followed by three numerical digits: the letter is"
            + "the first letter of the name, and the digits encode the remaining consonants."
            + "Given any string column value by the user as query parameter, return all the column values"
            + "which fall within 100 range for Soundex codes.",
            nickname = "getNameMatches",
            notes = "", response = NameMatchRs.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = NameMatchRs.class),
            @ApiResponse(code = 400, message = "Bad Request", response = BaseResponse.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = BaseResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = BaseResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = BaseResponse.class),
            @ApiResponse(code = 429, message = "Too Many Requests", response = BaseResponse.class),
            @ApiResponse(code = 500, message = "Internal Error", response = BaseResponse.class),
            @ApiResponse(code = 502, message = "Bad Gateway", response = BaseResponse.class),
            @ApiResponse(code = 503, message = "Service Unavailable", response = BaseResponse.class),
            @ApiResponse(code = 504, message = "Gateway Timeout", response = BaseResponse.class)})
    @GetMapping(value = "/match", produces = {"application/json"})
    @PreAuthorize("hasRole(@environment.getProperty('authority.group.user'))")
    public ResponseEntity<NameMatchRs> getNameMatches(
            @RequestParam(value = "firstName") String firstName) throws Exception {
        return new ResponseEntity(statsService.getNameMatch(firstName, getContext()), HttpStatus.OK);
    }

}
