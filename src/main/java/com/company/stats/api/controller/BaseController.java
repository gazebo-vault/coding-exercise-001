package com.company.stats.api.controller;

import com.company.stats.commons.CommonUtils;
import com.company.stats.core.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import javax.servlet.http.HttpServletRequest;

/**
 *
 */
public class BaseController {

    @Lazy
    @Autowired
    private HttpServletRequest httpServletRequest;

    /**
     * @return
     */
    protected Context getContext() {
        return CommonUtils.getContext(httpServletRequest);
    }

    /**
     * @return
     */
    public HttpServletRequest getHttpServletRequest() {
        return httpServletRequest;
    }

}
