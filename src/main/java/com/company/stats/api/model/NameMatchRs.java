package com.company.stats.api.model;

import com.company.stats.core.BaseResponse;

import java.util.Set;

public class NameMatchRs extends BaseResponse {

    private Set<String> output;

    public Set<String> getOutput() {
        return output;
    }

    public void setOutput(Set<String> output) {
        this.output = output;
    }
}
