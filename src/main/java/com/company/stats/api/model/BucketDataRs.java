package com.company.stats.api.model;

import com.company.stats.core.BaseResponse;

import java.util.List;

public class BucketDataRs extends BaseResponse {

    private String columnName;
    private List<BucketData> bucketData;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public List<BucketData> getBucketData() {
        return bucketData;
    }

    public void setBucketData(List<BucketData> bucketData) {
        this.bucketData = bucketData;
    }
}
