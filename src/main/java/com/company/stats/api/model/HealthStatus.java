package com.company.stats.api.model;

public class HealthStatus {

    private String status = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

