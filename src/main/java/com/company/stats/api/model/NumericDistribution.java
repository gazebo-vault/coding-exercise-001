package com.company.stats.api.model;

import java.math.BigDecimal;

public class NumericDistribution {

    private String columnName;
    private Integer count;
    private Integer distinctCount;
    private Integer min;
    private Integer max;
    private BigDecimal mean;
    private BigDecimal stddev;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getDistinctCount() {
        return distinctCount;
    }

    public void setDistinctCount(Integer distinctCount) {
        this.distinctCount = distinctCount;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public BigDecimal getMean() {
        return mean;
    }

    public void setMean(BigDecimal mean) {
        this.mean = mean;
    }

    public BigDecimal getStddev() {
        return stddev;
    }

    public void setStddev(BigDecimal stddev) {
        this.stddev = stddev;
    }
}
