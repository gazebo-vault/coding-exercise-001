package com.company.stats.api.model;

import java.util.List;

public class Statistics {

    private List<NumericDistribution> numericDistributions;

    public List<NumericDistribution> getNumericDistributions() {
        return numericDistributions;
    }

    public void setNumericDistributions(List<NumericDistribution> numericDistributions) {
        this.numericDistributions = numericDistributions;
    }
}
