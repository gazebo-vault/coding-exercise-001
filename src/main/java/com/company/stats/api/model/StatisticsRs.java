package com.company.stats.api.model;

import com.company.stats.core.BaseResponse;

public class StatisticsRs extends BaseResponse {

    private Statistics statistics;

    public Statistics getStatistics() {
        return statistics;
    }

    public void setStatistics(Statistics statistics) {
        this.statistics = statistics;
    }
}
