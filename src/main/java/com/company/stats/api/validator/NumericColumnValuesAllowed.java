package com.company.stats.api.validator;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Arrays;

/**
 *
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {NumericColumnValuesAllowed.NumericColumnValuesAllowedValidator.class})
public @interface NumericColumnValuesAllowed {

    /**
     *
     * @return
     */
    String message() default "Field value should be from list of ";

    /**
     *
     * @return
     */
    Class<?>[] groups() default {};

    /**
     *
     * @return
     */
    Class<? extends Payload>[] payload() default {};

    /**
     *
     * @return
     */
    String propName();

    /**
     *
     * @return
     */
    String[] values();

    /**
     *
     */
    class NumericColumnValuesAllowedValidator implements ConstraintValidator<NumericColumnValuesAllowed, Object> {

        private String propName;
        private String message;
        private String[] values;

        /**
         *
         * @param requiredIfChecked
         */
        @Override
        public void initialize(NumericColumnValuesAllowed requiredIfChecked) {
            propName = requiredIfChecked.propName();
            message = requiredIfChecked.message();
            values = requiredIfChecked.values();
        }

        /**
         *
         * @param object
         * @param context
         * @return
         */
        @Override
        public boolean isValid(Object object, ConstraintValidatorContext context) {
            Boolean valid = true;
            Object checkedValue = String.valueOf(object);
            if (checkedValue != null) {
                valid = Arrays.asList(values).contains(checkedValue.toString().toLowerCase());
            }
            if (!valid) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate(message.concat(Arrays.toString(values)))
                        .addConstraintViolation();
            }
            return valid;
        }
    }

}
