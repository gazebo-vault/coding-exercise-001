package com.company.stats;

import com.company.stats.commons.CommonUtils;
import com.company.stats.commons.Constants;
import com.company.stats.commons.ErrorConstants;
import com.company.stats.core.BaseResponse;
import com.company.stats.core.LoggerContext;
import com.company.stats.exception.AuthorizationException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.nio.file.AccessDeniedException;
import java.util.List;

/**
 *
 */
@ControllerAdvice
public class ApplicationExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationExceptionHandler.class);

    @Autowired
    private MessageHandler messageHandler;

    /**
     *
     */
    @Lazy
    @Autowired
    private HttpServletRequest httpServletRequest;

    /**
     * This Method Handles Internal Server Error.
     *
     * @param exception Exception
     * @return error ResponseEntity
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler
    public ResponseEntity handleInternalServerError(Exception exception) {
        BaseResponse baseResponse = getBaseResponse(ErrorConstants.UNKNOWN_ERROR_CODE, exception);
        return buildResponse(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR, exception);
    }

    /**
     * This Method Handles Forbidden Request.
     *
     * @param exception AccessDeniedException
     * @return baseResponse ResponseEntity
     */
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<BaseResponse> handleForbiddenRequest(AccessDeniedException exception) {
        BaseResponse baseResponse = getBaseResponse(ErrorConstants.FORBIDDEN_ERROR_CODE, exception);
        return buildResponse(baseResponse, HttpStatus.FORBIDDEN, exception);
    }

    /**
     * This Method Handles Authorization Exception.
     *
     * @param exception AuthorizationException
     * @return baseResponse ResponseEntity
     */
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(AuthorizationException.class)
    public ResponseEntity handleAuthorizationErrors(AuthorizationException exception) {
        BaseResponse baseResponse = getBaseResponse(exception.getErrorCode(), exception);
        return buildResponse(baseResponse, HttpStatus.FORBIDDEN, exception);
    }

    /**
     * This Method Handles Constraint Violation Exception Server Error.
     *
     * @param exception ConstraintViolationException
     * @return error ResponseEntity
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity handleConstraintViolationError(ConstraintViolationException exception) {
        String key = getConstraintViolationErrorKey(exception.getMessage());
        BaseResponse baseResponse = getBaseResponse(key, exception);
        return buildResponse(baseResponse, HttpStatus.BAD_REQUEST, exception);

    }

    /**
     * This Method Handles Unsupported Media Error.
     *
     * @param exception HttpMediaTypeNotSupportedException
     * @return error ResponseEntity
     */
    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseEntity handleHttpMediaTypeNotSupportedException(Exception exception) {
        BaseResponse baseResponse = getBaseResponse(ErrorConstants.INPUT_REQUEST_ERROR_CODE, exception);
        return buildResponse(baseResponse, HttpStatus.UNSUPPORTED_MEDIA_TYPE, exception);

    }

    /**
     * This Method Handles Servlet Binding Error.
     *
     * @param exception ServletRequestBindingException
     * @return error ResponseEntity
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ServletRequestBindingException.class)
    public ResponseEntity handleServletRequestBindingException(Exception exception) {
        BaseResponse baseResponse = getBaseResponse(ErrorConstants.INPUT_REQUEST_ERROR_CODE, exception);
        return buildResponse(baseResponse, HttpStatus.BAD_REQUEST, exception);

    }

    /**
     * This Method Handles Bad Request Error.
     *
     * @param exception MethodArgumentNotValidException
     * @return baseResponse ResponseEntity
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity handleBadRequest(MethodArgumentNotValidException exception) {
        String key;
        BaseResponse baseResponse;
        List<FieldError> fieldErrorList = exception.getBindingResult().getFieldErrors();
        if (!fieldErrorList.isEmpty()) {
            key = formatErrorKey(fieldErrorList.get(0).getField(), String.valueOf(HttpStatus.BAD_REQUEST.value()));
        } else {
            key = exception.getBindingResult() != null && exception.getBindingResult().getGlobalError() != null ?
                    formatErrorKey(exception.getBindingResult().getGlobalError().getCode(),
                            String.valueOf(HttpStatus.BAD_REQUEST.value())) : ErrorConstants.UNKNOWN_ERROR_CODE;
        }
        baseResponse = getBaseResponse(key, exception);
        ResponseEntity responseEntity = new ResponseEntity(baseResponse, HttpStatus.BAD_REQUEST);
        LoggerContext loggerContext = CommonUtils.loadLoggerContext(httpServletRequest);
        loggerContext.setApiResponse(responseEntity);
        if (exception.getBindingResult() != null && exception.getBindingResult().getTarget() != null) {
            loggerContext.setApiRequest(exception.getBindingResult().getTarget());
        }
        httpServletRequest.setAttribute(Constants.LOG_TYPE_ANALYTICS, loggerContext);
        logger.error(loggerContext.toString());
        return responseEntity;
    }

    /**
     * Get Base Response
     *
     * @param errorCode Error Code
     * @param exception Exception
     * @return baseResponse BaseResponse
     */
    private BaseResponse getBaseResponse(String errorCode, Exception exception) {
        BaseResponse baseResponse = messageHandler.getBaseError(errorCode, exception);
        return baseResponse;
    }


    /**
     * Build Response
     *
     * @param baseResponse Base Response
     * @param httpStatus   Http Status
     * @return responseEntity ResponseEntity
     */
    private ResponseEntity buildResponse(BaseResponse baseResponse, HttpStatus httpStatus, Exception excp) {
        ResponseEntity responseEntity = new ResponseEntity(baseResponse, httpStatus);
        LoggerContext loggerContext = CommonUtils.loadLoggerContext(httpServletRequest);
        loggerContext.setApiResponse(responseEntity);
        httpServletRequest.setAttribute(Constants.LOG_TYPE_ANALYTICS, loggerContext);
        logger.error(loggerContext.toString());
        logger.error("==========");
        logger.error("===============");
        logger.error("====================");
        logger.error(excp.getClass().toString() + " ---> { "
                + baseResponse.getError().getCode() + " } ", excp);
        logger.error("====================");
        logger.error("===============");
        logger.error("==========");
        return responseEntity;
    }

    /**
     * This method formats error keys.
     *
     * @param errorCode
     * @param httpStatus
     * @return
     */
    private String formatErrorKey(String errorCode, String httpStatus) {
        return String.format("%s.%s", errorCode, httpStatus);
    }

    /**
     * Get Constraint Violation Error Key
     *
     * @param key key
     * @return constraintViolationKey String
     */
    private String getConstraintViolationErrorKey(String key) {
        return StringUtils.contains(key, ":")
                ? StringUtils.substring(key, 0, key.indexOf(":")).trim() : key;
    }

}
