package com.company.stats.core;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Warning {
    @JsonProperty("code")
    private String code = null;

    @JsonProperty("message")
    private String message = null;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
