package com.company.stats.core;

import java.io.Serializable;

public class Context implements Serializable {

    private static final long serialVersionUID = 1L;

    private String transactionId;
    private String deviceId;
    private String application;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

}
