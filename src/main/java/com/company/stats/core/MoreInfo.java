package com.company.stats.core;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MoreInfo {
    @JsonProperty("code")
    private String code = null;

    @JsonProperty("field")
    private String field = null;

    @JsonProperty("fieldValue")
    private String fieldValue = null;

    @JsonProperty("message")
    private String message = null;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

