package com.company.stats.core;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Error {
    @JsonProperty("code")
    private String code = null;

    @JsonProperty("message")
    private String message = null;

    @JsonProperty("developerMessage")
    private String developerMessage = null;

    @JsonProperty("moreInfo")
    private List<MoreInfo> moreInfo = null;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDeveloperMessage() {
        return developerMessage;
    }

    public void setDeveloperMessage(String developerMessage) {
        this.developerMessage = developerMessage;
    }

    public List<MoreInfo> getMoreInfo() {
        return moreInfo;
    }

    public void setMoreInfo(List<MoreInfo> moreInfo) {
        this.moreInfo = moreInfo;
    }
}
