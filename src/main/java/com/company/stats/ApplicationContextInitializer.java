package com.company.stats;

import org.springframework.context.ConfigurableApplicationContext;

/**
 *
 */
public class ApplicationContextInitializer implements org.springframework.context
        .ApplicationContextInitializer<ConfigurableApplicationContext> {

    /**
     * @param applicationContext
     */
    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
    }
}
