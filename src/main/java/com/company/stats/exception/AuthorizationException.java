package com.company.stats.exception;

import java.util.Map;

public class AuthorizationException extends BaseRuntimeException{
    public AuthorizationException(String errorCode) {
        super(errorCode);
    }

    public AuthorizationException(String message, String errorCode) {
        super(message, errorCode);
    }

    public AuthorizationException(String message, String errorCode, Map<String, Object> apiErrorDataMap) {
        super(message, errorCode, apiErrorDataMap);
    }

    public AuthorizationException(String message, Throwable cause, String errorCode) {
        super(message, cause, errorCode);
    }

    public AuthorizationException(Throwable cause, String errorCode) {
        super(cause, errorCode);
    }

    public AuthorizationException(String message, Throwable cause, boolean enableSuppression, boolean
            writableStackTrace, String errorCode) {
        super(message, cause, enableSuppression, writableStackTrace, errorCode);
    }

    public AuthorizationException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthorizationException(Throwable cause, String errorCode, String message, String reason) {
        super(cause, errorCode, message, reason);
    }

    public AuthorizationException(Throwable cause, String errorCode, String message, String reason, Map<String, Object> apiErrorDataMap) {
        super(cause, errorCode, message, reason, apiErrorDataMap);
    }
}
