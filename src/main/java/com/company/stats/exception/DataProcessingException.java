package com.company.stats.exception;

import java.util.Map;

public class DataProcessingException extends BaseRuntimeException{
    public DataProcessingException(String errorCode) {
        super(errorCode);
    }

    public DataProcessingException(String message, String errorCode) {
        super(message, errorCode);
    }

    public DataProcessingException(String message, String errorCode, Map<String, Object> apiErrorDataMap) {
        super(message, errorCode, apiErrorDataMap);
    }

    public DataProcessingException(String message, String errorCode, String errorMessageType) {
        super(message, errorCode, errorMessageType);
    }

    public DataProcessingException(String message, Throwable cause, String errorCode) {
        super(message, cause, errorCode);
    }

    public DataProcessingException(Throwable cause, String errorCode) {
        super(cause, errorCode);
    }

    public DataProcessingException(String message, Throwable cause, boolean enableSuppression, boolean
            writableStackTrace, String errorCode) {
        super(message, cause, enableSuppression, writableStackTrace, errorCode);
    }

    public DataProcessingException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataProcessingException(Throwable cause, String errorCode, String message, String reason) {
        super(cause, errorCode, message, reason);
    }

    public DataProcessingException(Throwable cause, String errorCode, String message, String reason, Map<String, Object> apiErrorDataMap) {
        super(cause, errorCode, message, reason, apiErrorDataMap);
    }
}
