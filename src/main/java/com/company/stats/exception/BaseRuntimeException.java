package com.company.stats.exception;

import java.util.Map;

public class BaseRuntimeException extends RuntimeException {
    private String errorCode;
    private String errorMessage;
    private String reason;
    private String errorMessageType;

    public BaseRuntimeException(String errorCode) {
        this.errorCode = errorCode;
    }

    public BaseRuntimeException(String message, String errorCode) {
        super(message);
        this.errorMessage = message;
        this.errorCode = errorCode;
    }

    public BaseRuntimeException(String message, String errorCode,String errorMessageType) {
        super(message);
        this.errorMessage = message;
        this.errorCode = errorCode;
        this.errorMessageType = errorMessageType;
    }

    public BaseRuntimeException(String message, String errorCode, Map<String, Object> apiErrorDataMap) {
        super(message);
        this.errorMessage = message;
        this.errorCode = errorCode;
    }

    public BaseRuntimeException(String message, Throwable cause, String errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
        this.errorMessage = message;
    }

    public BaseRuntimeException(Throwable cause, String errorCode) {
        super(cause);
        this.errorCode = errorCode;
    }

    public BaseRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean
            writableStackTrace, String errorCode) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.errorMessage = message;
        this.errorCode = errorCode;
    }

    public BaseRuntimeException(String message, Throwable cause) {
        super(message, cause);
        this.errorMessage = message;
    }


    public BaseRuntimeException(Throwable cause, String errorCode, String message, String reason) {
        super(message, cause);
        this.errorCode = errorCode;
        this.errorMessage = message;
        this.reason = reason;
    }

    public BaseRuntimeException(Throwable cause, String errorCode, String message, String reason,
                                Map<String, Object> apiErrorDataMap) {
        super(message, cause);
        this.errorCode = errorCode;
        this.errorMessage = message;
        this.reason = reason;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getErrorMessageType() {
        return errorMessageType;
    }

    public void setErrorMessageType(String errorMessageType) {
        this.errorMessageType = errorMessageType;
    }
}