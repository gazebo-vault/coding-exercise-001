package com.company.stats.api.service;

import com.company.stats.ApplicationTest;
import com.company.stats.ApplicationTestUtility;
import com.company.stats.api.model.BucketDataRs;
import com.company.stats.api.model.NameMatchRs;
import com.company.stats.api.model.StatisticsRs;
import com.company.stats.core.Context;
import com.company.stats.service.StatsService;
import com.company.stats.service.helper.BucketDataResponseHelper;
import com.company.stats.service.helper.NameMatchResponseHelper;
import com.company.stats.service.helper.StatisticsResponseHelper;
import com.company.stats.service.workflow.StatsWorkflow;
import com.company.stats.service.workflow.domain.BucketDataContext;
import com.company.stats.service.workflow.domain.NameMatchContext;
import com.company.stats.service.workflow.domain.StatsContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
public class StatsServiceImplTest extends ApplicationTest {

    public static final String MOCK_STATISTICS_OK_RESPONSE_DATA_PATH = "classpath:mock/statistics_ok_resp.json";
    public static final String MOCK_BUCKETDATA_OK_RESPONSE_DATA_PATH = "classpath:mock/bucketdata_ok_resp.json";
    public static final String MOCK_MATCH_OK_RESPONSE_DATA_PATH = "classpath:mock/match_ok_resp.json";

    @Value(MOCK_STATISTICS_OK_RESPONSE_DATA_PATH)
    private Resource mockStatisticsOkResponse;

    @Value(MOCK_BUCKETDATA_OK_RESPONSE_DATA_PATH)
    private Resource mockBucketDataOkResponse;

    @Value(MOCK_MATCH_OK_RESPONSE_DATA_PATH)
    private Resource mockMatchOkResponse;

    @Autowired
    private ApplicationTestUtility applicationTestUtility;

    @MockBean
    private StatisticsResponseHelper statisticsResponseHelper;

    @MockBean
    private BucketDataResponseHelper bucketDataResponseHelper;

    @MockBean
    private NameMatchResponseHelper nameMatchResponseHelper;

    @MockBean
    private StatsWorkflow statsWorkflow;

    @Autowired
    private StatsService statsService;

    @Test
    public void testGetStatisticsWithValidResponse() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Mockito.when(statisticsResponseHelper.buildResponse(Mockito.any(), Mockito.any()))
                .thenReturn(mapper.readValue(ApplicationTestUtility.readContent(mockStatisticsOkResponse), StatisticsRs.class));
        Mockito.when(statsWorkflow.getStatistics(Mockito.any(), Mockito.any()))
                .thenReturn(mapper.readValue(ApplicationTestUtility.readContent(mockStatisticsOkResponse), StatsContext.class));
        assertNotNull(statsService.getStatistics(new Context()));
    }

    @Test
    public void testGetBucketDataWithValidResponse() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Mockito.when(bucketDataResponseHelper.buildResponse(Mockito.any(), Mockito.any()))
                .thenReturn(mapper.readValue(ApplicationTestUtility.readContent(mockBucketDataOkResponse), BucketDataRs.class));
        Mockito.when(statsWorkflow.getBucketData(Mockito.any(), Mockito.any()))
                .thenReturn(mapper.readValue(ApplicationTestUtility.readContent(mockBucketDataOkResponse), BucketDataContext.class));
        assertNotNull(statsService.getBucketData("age", 5, new Context()));
    }

    @Test
    public void testGetNameMatchWithValidResponse() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Mockito.when(nameMatchResponseHelper.buildResponse(Mockito.any(), Mockito.any()))
                .thenReturn(mapper.readValue(ApplicationTestUtility.readContent(mockMatchOkResponse), NameMatchRs.class));
        Mockito.when(statsWorkflow.getNameMatches(Mockito.any(), Mockito.any()))
                .thenReturn(mapper.readValue(ApplicationTestUtility.readContent(mockMatchOkResponse), NameMatchContext.class));
        assertNotNull(statsService.getNameMatch("darwin", new Context()));
    }
}
