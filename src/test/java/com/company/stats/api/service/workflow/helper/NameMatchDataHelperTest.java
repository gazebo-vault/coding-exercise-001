package com.company.stats.api.service.workflow.helper;

import com.company.stats.ApplicationTest;
import com.company.stats.ApplicationTestUtility;
import com.company.stats.commons.CommonUtils;
import com.company.stats.core.Context;
import com.company.stats.data.dto.UserDetail;
import com.company.stats.service.workflow.domain.NameMatchContext;
import com.company.stats.service.workflow.helper.NameMatchDataHelper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.LinkedHashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
public class NameMatchDataHelperTest extends ApplicationTest {

    public static final String MOCK_USER_DETAILS_PATH = "classpath:mock/user_details.json";

    @Value(MOCK_USER_DETAILS_PATH)
    private Resource mockUserDetails;

    @MockBean
    private NameMatchContext nameMatchContext;

    @Autowired
    private ApplicationTestUtility applicationTestUtility;

    @Autowired
    private NameMatchDataHelper nameMatchDataHelper;

    @Test
    public void testProcessSoundexNameMatching() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        List<LinkedHashMap> list = mapper.readValue(ApplicationTestUtility.readContent(mockUserDetails), List.class);
        List<UserDetail> userDetails = CommonUtils.transformCollection(list,
                new TypeReference<List<UserDetail>>() {
                });
        Mockito.when(nameMatchContext.getUserDetailList())
                .thenReturn(userDetails);
        Mockito.when(nameMatchContext.getFirstName()).thenReturn("darwin");
        assertNotNull(nameMatchDataHelper.processSoundexNameMatching(nameMatchContext, new Context()));
    }
}
