package com.company.stats.api.service.workflow;

import com.company.stats.ApplicationTest;
import com.company.stats.ApplicationTestUtility;
import com.company.stats.core.Context;
import com.company.stats.data.dao.UserDetailsDao;
import com.company.stats.service.workflow.StatsWorkflow;
import com.company.stats.service.workflow.domain.BucketDataContext;
import com.company.stats.service.workflow.domain.NameMatchContext;
import com.company.stats.service.workflow.domain.StatsContext;
import com.company.stats.service.workflow.helper.BucketDataHelper;
import com.company.stats.service.workflow.helper.NameMatchDataHelper;
import com.company.stats.service.workflow.helper.StatisticsDataHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
public class StatsWorkflowTest extends ApplicationTest {

    public static final String MOCK_STATISTICS_OK_RESPONSE_DATA_PATH = "classpath:mock/statistics_ok_resp.json";
    public static final String MOCK_BUCKETDATA_OK_RESPONSE_DATA_PATH = "classpath:mock/bucketdata_ok_resp.json";
    public static final String MOCK_MATCH_OK_RESPONSE_DATA_PATH = "classpath:mock/match_ok_resp.json";
    public static final String MOCK_USER_DETAILS_PATH = "classpath:mock/user_details.json";

    @Value(MOCK_STATISTICS_OK_RESPONSE_DATA_PATH)
    private Resource mockStatisticsOkResponse;

    @Value(MOCK_BUCKETDATA_OK_RESPONSE_DATA_PATH)
    private Resource mockBucketDataOkResponse;

    @Value(MOCK_MATCH_OK_RESPONSE_DATA_PATH)
    private Resource mockMatchOkResponse;

    @Value(MOCK_USER_DETAILS_PATH)
    private Resource mockUserDetails;

    @Autowired
    private ApplicationTestUtility applicationTestUtility;

    @MockBean
    private StatisticsDataHelper statisticsDataHelper;

    @MockBean
    private BucketDataHelper bucketDataHelper;

    @MockBean
    private NameMatchDataHelper nameMatchDataHelper;

    @MockBean
    private UserDetailsDao userDetailsDao;

    @Autowired
    private StatsWorkflow statsWorkflow;

    @Test
    public void testGetStatisticsWithValidResponse() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Mockito.when(userDetailsDao.getUserDetails())
                .thenReturn(mapper.readValue(ApplicationTestUtility.readContent(mockUserDetails), List.class));
        Mockito.when(statisticsDataHelper.processNumericDistribution(Mockito.any(), Mockito.any()))
                .thenReturn(mapper.readValue(ApplicationTestUtility.readContent(mockStatisticsOkResponse), StatsContext.class));
        assertNotNull(statsWorkflow.getStatistics(new StatsContext(), new Context()));
    }

    @Test
    public void testGetBucketDataWithValidResponse() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Mockito.when(userDetailsDao.getUserDetails())
                .thenReturn(mapper.readValue(ApplicationTestUtility.readContent(mockUserDetails), List.class));
        Mockito.when(bucketDataHelper.processBucketDistribution(Mockito.any(), Mockito.any()))
                .thenReturn(mapper.readValue(ApplicationTestUtility.readContent(mockBucketDataOkResponse), BucketDataContext.class));
        assertNotNull(statsWorkflow.getBucketData(new BucketDataContext(), new Context()));
    }

    @Test
    public void testGetNameMatchesWithValidResponse() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Mockito.when(userDetailsDao.getUserDetails())
                .thenReturn(mapper.readValue(ApplicationTestUtility.readContent(mockUserDetails), List.class));
        Mockito.when(nameMatchDataHelper.processSoundexNameMatching(Mockito.any(), Mockito.any()))
                .thenReturn(mapper.readValue(ApplicationTestUtility.readContent(mockMatchOkResponse), NameMatchContext.class));
        assertNotNull(statsWorkflow.getNameMatches(new NameMatchContext(), new Context()));
    }
}
