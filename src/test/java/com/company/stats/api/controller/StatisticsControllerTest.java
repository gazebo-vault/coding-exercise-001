package com.company.stats.api.controller;

import com.company.stats.ApplicationTest;
import com.company.stats.ApplicationTestUtility;
import com.company.stats.api.model.BucketDataRs;
import com.company.stats.api.model.NameMatchRs;
import com.company.stats.api.model.StatisticsRs;
import com.company.stats.service.StatsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
public class StatisticsControllerTest extends ApplicationTest {

    public static final String MOCK_STATISTICS_OK_RESPONSE_DATA_PATH = "classpath:mock/statistics_ok_resp.json";
    public static final String MOCK_BUCKETDATA_OK_RESPONSE_DATA_PATH = "classpath:mock/bucketdata_ok_resp.json";
    public static final String MOCK_MATCH_OK_RESPONSE_DATA_PATH = "classpath:mock/match_ok_resp.json";

    @Value(MOCK_STATISTICS_OK_RESPONSE_DATA_PATH)
    private Resource mockStatisticsOkResponse;

    @Value(MOCK_BUCKETDATA_OK_RESPONSE_DATA_PATH)
    private Resource mockBucketDataOkResponse;

    @Value(MOCK_MATCH_OK_RESPONSE_DATA_PATH)
    private Resource mockMatchOkResponse;

    @Autowired
    private ApplicationTestUtility applicationTestUtility;

    @MockBean
    private StatsService statsService;

    @Test
    @WithMockUser(username = "admin1", roles = {"StatsAdmin"})
    public void testGetStatisticsWithValidResponse() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        applicationTestUtility.mockAuthorization();
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/statistics")
                .contentType(MediaType.APPLICATION_JSON)
                .headers(ApplicationTestUtility.setHeaders());
        Mockito.when(statsService.getStatistics(Mockito.any()))
                .thenReturn(mapper.readValue(ApplicationTestUtility.readContent(mockStatisticsOkResponse), StatisticsRs.class));
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        StatisticsRs value = mapper.readValue(result.getResponse().getContentAsString(), StatisticsRs.class);
        assertNotNull(value);
    }

    @Test
    @WithMockUser(username = "user1", roles = {"StatsUser"})
    public void testGetBucketWithValidResponse() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        applicationTestUtility.mockAuthorization();
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/bucketdata?columnName=age&bucket=5")
                .contentType(MediaType.APPLICATION_JSON)
                .headers(ApplicationTestUtility.setHeaders());
        Mockito.when(statsService.getBucketData(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(mapper.readValue(ApplicationTestUtility.readContent(mockBucketDataOkResponse), BucketDataRs.class));
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        BucketDataRs value = mapper.readValue(result.getResponse().getContentAsString(), BucketDataRs.class);
        assertNotNull(value);
    }

    @Test
    @WithMockUser(username = "user1", roles = {"StatsUser"})
    public void testGetMatchWithValidResponse() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        applicationTestUtility.mockAuthorization();
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/match?firstName=darwin")
                .contentType(MediaType.APPLICATION_JSON)
                .headers(ApplicationTestUtility.setHeaders());
        Mockito.when(statsService.getNameMatch(Mockito.any(), Mockito.any()))
                .thenReturn(mapper.readValue(ApplicationTestUtility.readContent(mockMatchOkResponse), NameMatchRs.class));
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        NameMatchRs value = mapper.readValue(result.getResponse().getContentAsString(), NameMatchRs.class);
        assertNotNull(value);
    }
}
