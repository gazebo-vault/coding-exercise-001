package com.company.stats;

import com.company.stats.commons.ErrorConstants;
import com.company.stats.exception.DataProcessingException;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
public class MessageHandlerTest extends ApplicationTest {

    @Autowired
    private MessageHandler messageHandler;

    @Test
    void tetGet() throws Exception {
        assertNotNull(messageHandler.get("400"));
    }

    @Test
    void testGetValidationErrorCode() {
        assertNotNull(messageHandler.getValidationErrorCode("getBucketData.columnName"));
    }

    @Test
    void testGetBaseError() {
        assertNotNull(messageHandler.getBaseError("400",
                new DataProcessingException(ErrorConstants.EMPTY_RESULT_ERROR_CODE)));
    }
}
