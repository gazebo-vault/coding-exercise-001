package com.company.stats;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.stream.Collectors;

@Configuration
public class ApplicationTestUtility {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationTestUtility.class);

    @PostConstruct
    public void mockAuthorization() {
        MockitoAnnotations.initMocks(this);
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (IOException e) {
            logger.error("{} {}", "Exception Message::", e.getMessage());
        }
        return null;
    }

    public static HttpHeaders setHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("transactionId", "12345");
        httpHeaders.set("deviceId", "ABCDE");
        httpHeaders.set("application", "credit-report");
        return httpHeaders;
    }

    public static String readContent(Resource resourceFile) {
        String content = null;
        try {
            InputStream inputStream = Objects.nonNull(resourceFile) ? resourceFile.getInputStream() : null;
            content = new BufferedReader(
                    new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                    .lines()
                    .collect(Collectors.joining("\n"));
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return content;
    }
}
