package com.company.stats;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.Assert.assertNotNull;

@WebAppConfiguration
@AutoConfigureMockMvc
@SpringBootTest(classes = Application.class)
@ComponentScan({"com.company.stats"})
public class ApplicationTest {

    @Autowired
    protected MockMvc mockMvc;

    static {
        System.setProperty("environment", "dev");
    }

    @Test
    public void testMockMvc() {
        assertNotNull(mockMvc);
    }

}
